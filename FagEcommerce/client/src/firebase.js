// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDU2W30hyibqSDxcoo5Bmoq1u3J8YryRSQ",
  authDomain: "fagecommerce.firebaseapp.com",
  projectId: "fagecommerce",
  storageBucket: "fagecommerce.appspot.com",
  messagingSenderId: "772560313514",
  appId: "1:772560313514:web:7817d62f2f7ad7879f84d3",
  measurementId: "G-DCM4EQL5LJ"
};

// Initialize Firebase
//const app = initializeApp(firebaseConfig);
const app = firebase.initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

//export
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
export const microsoftAuthprovider = new firebase.auth.OAuthProvider('microsoft.com');