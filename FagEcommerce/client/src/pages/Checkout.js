import React,{useEffect,useState} from 'react'
import {useSelector,useDispatch} from 'react-redux'
import {getUserCart,emptyUserCart,saveUserAddress,applyCoupon,createCashOrderForUser} from "../functions/user"
import {toast} from 'react-toastify'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import {Input} from 'antd'

const {TextArea } =Input


const initialState = {
    postalCode: '',
    phoneNumber: '',
    country: 'México',
    states: [
        {
            "name": "Distrito Federal",
            "code": "MX-DIF",
            "subdivision": "federal district"
        },
        {
            "name": "Aguascalientes",
            "code": "MX-AGU",
            "subdivision": "state"
        },
        {
            "name": "Baja California",
            "code": "MX-BCN",
            "subdivision": "state"
        },
        {
            "name": "Baja California Sur",
            "code": "MX-BCS",
            "subdivision": "state"
        },
        {
            "name": "Campeche",
            "code": "MX-CAM",
            "subdivision": "state"
        },
        {
            "name": "Chiapas",
            "code": "MX-CHP",
            "subdivision": "state"
        },
        {
            "name": "Chihuahua",
            "code": "MX-CHH",
            "subdivision": "state"
        },
        {
            "name": "Coahuila",
            "code": "MX-COA",
            "subdivision": "state"
        },
        {
            "name": "Colima",
            "code": "MX-COL",
            "subdivision": "state"
        },
        {
            "name": "Durango",
            "code": "MX-DUR",
            "subdivision": "state"
        },
        {
            "name": "Guanajuato",
            "code": "MX-GUA",
            "subdivision": "state"
        },
        {
            "name": "Guerrero",
            "code": "MX-GRO",
            "subdivision": "state"
        },
        {
            "name": "Hidalgo",
            "code": "MX-HID",
            "subdivision": "state"
        },
        {
            "name": "Jalisco",
            "code": "MX-JAL",
            "subdivision": "state"
        },
        {
            "name": "Michoacán",
            "code": "MX-MIC",
            "subdivision": "state"
        },
        {
            "name": "Morelos",
            "code": "MX-MOR",
            "subdivision": "state"
        },
        {
            "name": "México",
            "code": "MX-MEX",
            "subdivision": "state"
        },
        {
            "name": "Nayarit",
            "code": "MX-NAY",
            "subdivision": "state"
        },
        {
            "name": "Nuevo León",
            "code": "MX-NLE",
            "subdivision": "state"
        },
        {
            "name": "Oaxaca",
            "code": "MX-OAX",
            "subdivision": "state"
        },
        {
            "name": "Puebla",
            "code": "MX-PUE",
            "subdivision": "state"
        },
        {
            "name": "Querétaro",
            "code": "MX-QUE",
            "subdivision": "state"
        },
        {
            "name": "Quintana Roo",
            "code": "MX-ROO",
            "subdivision": "state"
        },
        {
            "name": "San Luis Potosí",
            "code": "MX-SLP",
            "subdivision": "state"
        },
        {
            "name": "Sinaloa",
            "code": "MX-SIN",
            "subdivision": "state"
        },
        {
            "name": "Sonora",
            "code": "MX-SON",
            "subdivision": "state"
        },
        {
            "name": "Tabasco",
            "code": "MX-TAB",
            "subdivision": "state"
        },
        {
            "name": "Tamaulipas",
            "code": "MX-TAM",
            "subdivision": "state"
        },
        {
            "name": "Tlaxcala",
            "code": "MX-TLA",
            "subdivision": "state"
        },
        {
            "name": "Veracruz",
            "code": "MX-VER",
            "subdivision": "state"
        },
        {
            "name": "Yucatán",
            "code": "MX-YUC",
            "subdivision": "state"
        },
        {
            "name": "Zacatecas",
            "code": "MX-ZAC",
            "subdivision": "state"
        }
    ],
    state:'',
    colors:["Gris","Plateado","Dorado","Verde","Blanco","Negro","Rojo","Azul","Azul Obscuro"],
    city: '',
    client:'',
    color:'',
    address:''
}

const Checkout = ({history}) => {


   

    const [products,setProducts] = useState([])
    const [total,setTotal] = useState(0)
    const [coupon,setCoupon] = useState('')

    //discount price
    const [totalAfterDiscount,setTotalAfterDiscount] = useState(0)
    const [discountError,setDiscountError] = useState('')
   

    //const [name,setName] = useState("")
    //const [address,setAddress] = useState("")
    //const [edo,setEdo] = useState(["TOLUCA","QUERETARO"])
    /*const [postalCode,setPostalCode] = useState('')
    const [phoneNumber,setPhoneNumber] = useState('')
    const [country,setCountry] = useState("México")
    const [city,setCity] = useState("")
    const [state,setState] = useState([
        {
            "name": "Distrito Federal",
            "code": "MX-DIF",
            "subdivision": "federal district"
        },
        {
            "name": "Aguascalientes",
            "code": "MX-AGU",
            "subdivision": "state"
        },
        {
            "name": "Baja California",
            "code": "MX-BCN",
            "subdivision": "state"
        },
        {
            "name": "Baja California Sur",
            "code": "MX-BCS",
            "subdivision": "state"
        },
        {
            "name": "Campeche",
            "code": "MX-CAM",
            "subdivision": "state"
        },
        {
            "name": "Chiapas",
            "code": "MX-CHP",
            "subdivision": "state"
        },
        {
            "name": "Chihuahua",
            "code": "MX-CHH",
            "subdivision": "state"
        },
        {
            "name": "Coahuila",
            "code": "MX-COA",
            "subdivision": "state"
        },
        {
            "name": "Colima",
            "code": "MX-COL",
            "subdivision": "state"
        },
        {
            "name": "Durango",
            "code": "MX-DUR",
            "subdivision": "state"
        },
        {
            "name": "Guanajuato",
            "code": "MX-GUA",
            "subdivision": "state"
        },
        {
            "name": "Guerrero",
            "code": "MX-GRO",
            "subdivision": "state"
        },
        {
            "name": "Hidalgo",
            "code": "MX-HID",
            "subdivision": "state"
        },
        {
            "name": "Jalisco",
            "code": "MX-JAL",
            "subdivision": "state"
        },
        {
            "name": "Michoacán",
            "code": "MX-MIC",
            "subdivision": "state"
        },
        {
            "name": "Morelos",
            "code": "MX-MOR",
            "subdivision": "state"
        },
        {
            "name": "México",
            "code": "MX-MEX",
            "subdivision": "state"
        },
        {
            "name": "Nayarit",
            "code": "MX-NAY",
            "subdivision": "state"
        },
        {
            "name": "Nuevo León",
            "code": "MX-NLE",
            "subdivision": "state"
        },
        {
            "name": "Oaxaca",
            "code": "MX-OAX",
            "subdivision": "state"
        },
        {
            "name": "Puebla",
            "code": "MX-PUE",
            "subdivision": "state"
        },
        {
            "name": "Querétaro",
            "code": "MX-QUE",
            "subdivision": "state"
        },
        {
            "name": "Quintana Roo",
            "code": "MX-ROO",
            "subdivision": "state"
        },
        {
            "name": "San Luis Potosí",
            "code": "MX-SLP",
            "subdivision": "state"
        },
        {
            "name": "Sinaloa",
            "code": "MX-SIN",
            "subdivision": "state"
        },
        {
            "name": "Sonora",
            "code": "MX-SON",
            "subdivision": "state"
        },
        {
            "name": "Tabasco",
            "code": "MX-TAB",
            "subdivision": "state"
        },
        {
            "name": "Tamaulipas",
            "code": "MX-TAM",
            "subdivision": "state"
        },
        {
            "name": "Tlaxcala",
            "code": "MX-TLA",
            "subdivision": "state"
        },
        {
            "name": "Veracruz",
            "code": "MX-VER",
            "subdivision": "state"
        },
        {
            "name": "Yucatán",
            "code": "MX-YUC",
            "subdivision": "state"
        },
        {
            "name": "Zacatecas",
            "code": "MX-ZAC",
            "subdivision": "state"
        }
    ])*/

   

    const [values,setValues] = useState(initialState)

    //const [state,setState] = useState(["ESTADO 1","ESTADO 2"])

    const {postalCode,phoneNumber,country,states,colors,city,client,color,state,address} = values

    const [addressSaved,setAddressSaved] = useState(false)

    const dispatch = useDispatch()
    const {user,cod} = useSelector((state) => ({...state}))
    const couponTrueOrFalse = useSelector((state) => (state.coupon)) //this is to avoid issues with the redclare names in thsi case coupon

    useEffect(() => {

        getUserCart(user.token)
        .then((res) => {
            console.log("USER CART RESPONSE",JSON.stringify(res.data, null, 4))
            setProducts(res.data.products)
            setTotal(res.data.cartTotal)
        })

    },[])


    const handleChange = (e) => {
        e.preventDefault()

        /*setPostalCode({[e.target.name]:e.target.value})
        setPhoneNumber({[e.target.name]:e.target.value})
        setCity({[e.target.name]:e.target.value})
        setState({[e.target.name]:e.target.value})*/

        setValues({...values,[e.target.name]: e.target.value})

        console.log(e.target.name," ----- ",e.target.value)

    }


    const saveAddressToDb = () =>{

        //console.log(address)
        saveUserAddress(user.token,values)
        .then((res) => {

                console.log("SAVE ADDRESS SUCCESFULLY ----->",res)
                setAddressSaved(true)
                toast.success("Detalles de Entrega Guardados correctamente")
            

        }).catch((err) =>{
            console.log("SAVE ADDRESS GFAILED ----->",err)
        })

    }


    const emptyCart = () =>{

        //empty cart from local storage
        if(typeof(window) !== 'undefined')
        {
            localStorage.removeItem("cart")
        }

        //remove from redux
        dispatch({
            type:'ADD_TO_CART',
            payload: []
        });


        //remove from backend
        emptyUserCart(user.token)
        .then((res) => {
            setProducts([])
            setTotal(0)
            setTotalAfterDiscount(0)
            setCoupon('')
            setDiscountError('')
            toast.success("El carrito esta vacío, continua comprando")
        })

    }

    //const stateName = state.map(s => <option key={s} value={s}>{s}</option>)

    const applyDiscountCoupon = () =>{
        console.log("SEND COUPON TO BACKEND TO VERIFY THAT EXIST",coupon)

        applyCoupon(user.token,coupon)
        .then((res) => {
            console.log("RESPONSE ON COUPON APPLY",res.data)

            if(res.data)
            {
                setTotalAfterDiscount(res.data)
                
                //push the total Discount to redux store
                //update redux coupon applied
                dispatch({
                    type:"COUPON_APPLIED",
                    payload: true
                })

            }

            if(res.data.err)
            {
                setDiscountError(res.data.err)

               

                //update redux coupon applied
                dispatch({
                    type:"COUPON_APPLIED",
                    payload: false
                })
            }
        })
    }

    const showAddress = () =>{

        return(
            <>

                {/*<ReactQuill theme="snow" placeholder='Porfavor escribe tu domicilio para entregarte tu producto...'  value={address} onChange={setAddress}  />*/}
                { /*<h6>detalle</h6>
                        <input type="text" name='address' value={address} onChange={handleChange} />
                   <br/>*/}

                    <TextArea placeholder='Escribe tu domicilio...(colonia, calles, numero interioro exterior,etc...)' rows={4} name="address" value={address} onChange={handleChange} />
                   
                    <div className='col-md-3 row'>
                        <h6>código postal</h6>
                        <input type="number" name="postalCode" value={postalCode} onChange={handleChange}   />
                    </div>

                    <div className='col-md-3 row'>
                        <h6>Numero Telefonico</h6>
                        <input type="text" name="phoneNumber" value={phoneNumber} onChange={handleChange}   />
                    </div>

                    <div className='col-md-3 row'>
                        <h6>País</h6>
                        <input type="text" name="country" value={country} onChange={handleChange} disabled />
                    </div>


                    <div className="form-group">
                        <label>Estado</label>
                        <select value={state} name="state" className="form-control" onChange={handleChange}>
                        <option>Please select</option>
                        {states.map((s) => (
                            <option key={s} value={s.name}>
                            {s.name}
                            </option>
                        ))}
                        </select>
                    </div>

                   

                    <div className='col-md-3 row'>
                        <h6>Ciudad</h6>
                        <input type="text" name='city' value={city} onChange={handleChange} />
                    </div>

                    <div className='col-md-3 row'>
                        <h6>Nombre</h6>
                        <input type="text" name='client' value={client} onChange={handleChange} />
                    </div>

                    <button className='btn btn-primary mt-2 ' onClick={saveAddressToDb}>
                    Guardar
                    </button>
            
            </>
        )

    }

    const showProductSummary = () =>{


        return(

            <>

                {products.map((p,i) => (
                    <div key={i} >
                        <p>{p.product.title} ({p.color}) x {p.count}  = {" "}
                        {p.product.price * p.count} </p>
                    </div>
                ))}

            
            </>
        )


    }

    const showApplyCoupon = () => {
        
        return(<>

            <input type="text" className='form-control' value={coupon} onChange={(e) => {
                setCoupon(e.target.value)
                setDiscountError("")
                }} />
 
            <button onClick={applyDiscountCoupon} className='btn btn-primary mt-2' disabled={coupon == ''} >Aplicar</button>
        
        </>)
    }

    const createCashOrder = () =>{

        createCashOrderForUser(user.token,cod,couponTrueOrFalse)
        .then((res) => {

            console.log("COD response ---> ",res)

            //empty cart from redux,local storage,reset coupon,reset COD ,redirect
            if(res.data.ok)
            {
                //empty cart from local storage
                if(typeof(window) !== 'undefined')
                {
                    localStorage.removeItem("cart")
                }

                //empty redux cart
                dispatch({
                    type:"ADD_TO_CART",
                    payload:[]
                });

                //empty redux coupon
                dispatch({
                    type:"COUPON_APPLIED",
                    payload:false
                });

                //empty redux COD
                dispatch({
                    type:"COD",
                    payload:false
                });

                //empty cart from backend
                emptyUserCart(user.token)

                //redirect
                setTimeout(() =>{
                    history.push("/user/history")
                },1000)


            }

        }).catch((err) =>{
            console.log("COD ERROR ---> ",err)
        })

    }


    return(
        <>
            <div className='row'>
                <div className='col-md-6'>
                    <h4>Detalles de Entrega</h4>
                    <br/>
                    
                   {showAddress()}

                   
                    <hr/>
                    <h4>Tienes un codigo de descuento?</h4>
                    <br/>
                    {showApplyCoupon()}
                    <br/>
                    <br/>
                    {discountError && <p className='bg-danger p-2'>{discountError}</p>}
                    
                </div>


            <div className='col-md-6'>
                <h4>
                    Order Summary
                
                    
                </h4>
                <hr/>
                <p>Productos {products.length}</p>
                <hr/>
                {showProductSummary()}
                <hr/>
                <p>Total: $ {total} MXN</p>

                {totalAfterDiscount > 0 && (
                    <p className='bg-success p-2'>Descuento Aplicado: Total a Pagar: $ {totalAfterDiscount} MXN.</p>
                )}

                <div className='row'>
                    <div className='col-md-6'>
                       {cod ? 
                       (<button className='btn btn-primary' disabled={!addressSaved || !products.length} onClick={createCashOrder}  >Realizar Compra</button>) 
                       : (<button className='btn btn-primary' disabled={!addressSaved || !products.length} onClick={() => history.push("/payment")}  >Realizar Compra</button>)}
                    </div>
                    <br/>
                    <br/>
                    <div className='col-md-6'>
                       <button disabled={!products.length} onClick={emptyCart} className='btn btn-primary'>Vaciar Carrito</button>
                    </div>
                    
                </div>

            </div>

            </div>
        </>
    )
}

export default Checkout