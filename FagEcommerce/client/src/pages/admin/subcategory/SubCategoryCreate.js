import React,{useState,useEffect} from "react";
import AdminNav from "../../../components/nav/AdminNav"
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {getCategories} from '../../../functions/category'
import {createSubCategory,getSubCategories,removeSubCategory} from "../../../functions/subcategory"
import { Link } from "react-router-dom";
import {EditOutlined,DeleteOutlined} from '@ant-design/icons'
import SubCategoryForm from "../../../components/forms/SubCategoryForm";
import LocalSearch from "../../../components/forms/LocalSearch";


const SubCategoryCreate = () => {

    const [name,setName] = useState("")
    const [loading,setLoading] = useState(false)
    const [subcategories,setSubCategories] = useState([])
    const [categories,setCategories] = useState([])
    const [category,setCategory] = useState("")

    const {user} = useSelector(state =>( {...state}))

    //searching filtering
    const [keyword,setKeyword] = useState("")

    useEffect(() => {
        loadSubCategories()
        loadCategories()
        
    },[])

    const loadSubCategories = () => {
        getSubCategories()
        .then((res) => {
            setSubCategories(res.data)
        }).catch((err) => {
            toast.error(`Hubo un error al cargar las sub categorías existentes`)
            toast.error(err.response.data)
        })
    }

    const loadCategories = () => {
        getCategories()
        .then((res) => {
            setCategories(res.data)
        }).catch((err) => {
            toast.error(`Hubo un error al cargar las categorías existentes`)
            toast.error(err.response.data)
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true)

        createSubCategory({name,parent: category},user.token)
        .then((res) => {
            setLoading(false)
            setName('')
            toast.success(`La sub categoría ${res.data.name} se creó exitosamente`)
            loadSubCategories()
        }).catch((err) => {
            setLoading(false)
            if(err.response.status === 400)
            {
                toast.error(`la sub categoría ${name} no se pudo crear,verifique que tenga una categoría padre `)
                toast.error(err.response.data)
            }
            
        })

    }

    const handleRemove = async(slug) => {
        if( window.confirm(`Borrar la categoría ${slug} ?`))
        {
            setLoading(true)
            removeSubCategory(slug,user.token)
            .then((res) =>{
                setLoading(false)
                toast.success(`La sub categoría ${res.data.name} se eliminó correctamente`)
                loadSubCategories()
            }).catch((err) => {
                setLoading(false)
                toast.error(`La sub categoría ${slug} no se pudo eliminar`)
                if(err.response.status === 400)
                {   
                    setLoading(false)
                    toast.error(err.response.data)
                }
            })
        }
    }

    

    //setp 4
    const searched = (keyword) => (c) => c.name.toLowerCase().includes(keyword)
   

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                    {loading ? <h4 className="text-danger">Cargando...</h4> :  <h4>Crear Sub Categoría</h4>}
                     <br/>


                    <div className="form-group">
                        <h4>Categoría</h4>
                        
                        <select name="category" className="form-select" onChange={e => setCategory(e.target.value)}>
                            <option>Selecciona una opción...</option>
                           {categories.length > 0 && categories.map((c) => {
                                return(
                                    <option key={c._id} value={c._id}>{c.name}</option>
                                )
                           })}
                        </select>
                    </div>

                    

                    <br/>

                     <SubCategoryForm handleSubmit={handleSubmit} name={name} setName={setName}  />

                    {/*Step 2 and Step 3*/}
                    <br/>
                    <LocalSearch keyword={keyword} setKeyword={setKeyword} />


                     <hr/>
                     {/*{categories.length}
                     {JSON.stringify(categories)}*/}

                     {/*Step 5*/}
                    {subcategories.filter(searched(keyword)).map((s) => (
                        <div className="alert alert-secondary" key={s._id}>
                            {s.name}
                            <span onClick={() => handleRemove(s.slug)} className="btn btn-lg "><DeleteOutlined className="text-danger"  /></span>
                            <Link to={`/admin/subcategory/${s.slug}`}> <span className="btn btn-lg "><EditOutlined className="text-primary" /></span>{" "}</Link>
                        </div>
                    ))}

                </div>
            </div>  
        </div>
    )
}

export default SubCategoryCreate