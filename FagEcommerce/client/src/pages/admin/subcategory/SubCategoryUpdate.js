import React,{useState,useEffect} from "react";
import AdminNav from "../../../components/nav/AdminNav"
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {getCategories} from '../../../functions/category'
import {getSubCategory,updateSubCategory} from "../../../functions/subcategory"
import SubCategoryForm from "../../../components/forms/SubCategoryForm";


const SubCategoryUpdate = ({history,match}) => {

    const [name,setName] = useState("")
    const [loading,setLoading] = useState(false)
    const [categories,setCategories] = useState([])
    const [parent,setParent] = useState("")

    const {user} = useSelector(state =>( {...state}))

    useEffect(() => {
        loadCategories()
        loadSubCategory()
        
        
    },[])

    const loadSubCategory = () => {
        getSubCategory(match.params.slug)
        .then((res) => {
            setName(res.data.name)
            setParent(res.data.parent)
        }).catch((err) => {
            toast.error(`Hubo un error al cargar las sub categoría existente`)
            toast.error(err.response.data)
        })
    }

    const loadCategories = () => {
        getCategories()
        .then((res) => {
            setCategories(res.data)
        }).catch((err) => {
            toast.error(`Hubo un error al cargar las categorías existentes`)
            toast.error(err.response.data)
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true)

        updateSubCategory(match.params.slug,{name,parent},user.token)
        .then((res) => {
            setLoading(false)
            setName('')
            toast.success(`La sub categoría ${res.data.name} se actualizó exitosamente`)

            history.push('/admin/subcategory')

        }).catch((err) => {
            setLoading(false)
            if(err.response.status === 400)
            {
                toast.error(`la sub categoría ${name} no se pudo actualizar `)
                toast.error(err.response.data)
            }
            
        })

    }

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                    {loading ? <h4 className="text-danger">Cargando...</h4> :  <h4>Editar Sub Categoría</h4>}
                     <br/>


                    <div className="form-group">
                        <h4>Categoría</h4>
                        
                        <select name="category" className="form-select" onChange={e => setParent(e.target.value)} >
                           {categories.length > 0 && categories.map((c) => {
                                return(
                                    <option key={c._id} value={c._id} selected={c._id === parent} >{c.name}</option>
                                )
                           })}
                        </select>
                    </div>

                    

                    <br/>

                     <SubCategoryForm handleSubmit={handleSubmit} name={name} setName={setName}  />

                   
                     <hr/>
                     

                </div>
            </div>  
        </div>
    )
}

export default SubCategoryUpdate