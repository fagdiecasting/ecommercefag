import React,{useState,useEffect} from "react";
import AdminNav from "../../../components/nav/AdminNav";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { EditOutlined,DeleteOutlined,LoadingOutlined} from "@ant-design/icons";
import ColorForm from '../../../components/forms/ColorForm'
import { createColor,updateColor,removeColor,getColor,getColors } from "../../../functions/color";


const ColorCreate = () => {

    const [loading,setLoading] = useState(false)
    const [name,setName] = useState("")
    const [colors,setColors] = useState([])

    const {user} = useSelector((state) => ({...state}))

    useEffect(() => {
        loadColors()
    },[])

    const loadColors = () => {

        setLoading(true)

        getColor()
        .then((res) => {
            setColors(res.data)
            setLoading(false)
        }).catch((err) =>{
            toast.error(`Hubo un error al cargar los colors`,err.response.data)
        })


    }


    const handleSubmit  = (e) => {

        e.preventDefault()

        setLoading(true)


        createColor({name},user.token)
        .then((res) =>{
            setLoading(false)
            setName('')
            toast.success(`El color ${res.data.color} se agrego con éxito a la base de datos`)
            loadColors()
        }).catch((err) =>{
            toast.error(`El color  no se pudo agregar  a la base de datos ${err.response.data}`)
        })

    }

    return(
        <>

        <div className="container-fluid">
            <div className="row">
                <div className="col-md-2">
                    <AdminNav />
                </div>

                <div className="col">

                <ColorForm handleSubmit={handleSubmit} name={name} setName={setName} />

                </div>



            </div>  
        </div>
        
        </>
    )

}

export default ColorCreate