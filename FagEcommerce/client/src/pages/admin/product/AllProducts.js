import React,{useEffect,useState} from "react";
import AdminNav from "../../../components/nav/AdminNav"
import { getProductsByCount,removeProduct } from "../../../functions/product";
import { FlagFilled, LoadingOutlined } from "@ant-design/icons";
import AdminProductCard from "../../../components/cards/AdminProductCard";
import { toast } from "react-toastify";
import {useSelector} from 'react-redux'
import axios from 'axios'


const AllProducts = () => {

    const [products,setProducts] = useState([])
    const [loading,setLoading] = useState(false)

    const {user} = useSelector((state) => ({...state}))

    useEffect(() => {
        loadAllProducts()
    },[])

    const loadAllProducts = () => {

        setLoading(true)

        getProductsByCount(100)
        .then((res) => {
            //console.log(res.data)
            setProducts(res.data)
            setLoading(false)
        })
        .catch((err) => {
            console.log(err)
            setLoading(false)
        })
    }


    const handleImageRemove = (id) => {
        axios.post( `${process.env.REACT_APP_API}/removeimage`,{public_id: id},{
            headers:{
                authtoken: user ? user.token : ""
            }
        }).then((res) => {

            setLoading(false)

            const {images} = products

            let filteredImages = images.filter((item) => {
                return item.public_id !== id
            });

            setProducts({...products,images: filteredImages})
        }).catch((err) => {
            setLoading(false)
            console.log(err)
        })

    }


    const handleRemoveProduct = (slug) => {
    
        const alert = window.confirm('Eliminar? ')
        

        if(alert)
        {
            setLoading(true)

            //handleImageRemove(products._id);

            removeProduct(slug,user.token)
            .then((res) => {

                setLoading(false)
                loadAllProducts()
                toast.success(`El producto ${res.data.title} se elimino correctamente`)

            }).catch((err) => {
                setLoading(false)

                if(err.response.status === 400)
                {
                    toast.error(`No se pudo eliminar el producto ${err.response.data}`)
                }
                
            })
        }

        


    }


    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                    
                    <h5>Todos los Productos</h5>
                    <br/>

                   { loading ? <LoadingOutlined className="text-danger h1" /> :( <div className="col">
                       <div className="row">
                       {products.map((p) => (

                            <div key={p._id} className="col-md-4 pb-3">

                                <AdminProductCard product={p} handleRemoveProduct={handleRemoveProduct} />
                            </div>

                        ))}
                       </div>
                   </div>)}

                  


                </div>
            </div>  
        </div>
    )
}

export default AllProducts