import React,{useState,useEffect} from 'react'
import {AdminProductCard} from '../../../components/cards/AdminProductCard'
import AdminNav from "../../../components/nav/AdminNav"
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {getProduct, updateProduct} from "../../../functions/product"
import { Link } from "react-router-dom";
import {EditOutlined,DeleteOutlined} from '@ant-design/icons'
import ProductUpdateForm from "../../../components/forms/ProductUpdateForm";
import LocalSearch from "../../../components/forms/LocalSearch";
import {getCategories,getCategorySubcategories} from "../../../functions/category"
import FileUploadForm from "../../../components/forms/FileUploadForm";
import {LoadingOutlined} from '@ant-design/icons'
import { useParams } from 'react-router-dom';



const initialState = {
    title:'',
    description:'',
    price:'',
    category:'',
    subcategories:[],
    shipping:'',
    quantity:'',
    images:[],
    //colors:["Gris","Plateado","Dorado","Verde","Blanco","Negro","Rojo","Azul","Azul Obscuro"],
    colors:["Color Base"],
    brands:["Die Casting","Marca 2","Marca 3","Marca 4"],
    color:'',
    brand:''
}




const ProductUpdate = ({match,history}) => {

    //state
    const [values,setValues] = useState(initialState)
    
    const [categories,setCategories] = useState([])

    const [subcategoryOptions,setSubCategoryOptions] = useState([])

    const [arrayOfSubcategories,setArrayOfSubcategoriesId] = useState([])

    const [selectedCategory,setSelectedCategory] = useState("")

    const [loading,setLoading] = useState(false)

    const {user} = useSelector(state =>( {...state}))

    //router
    //let {slug} = useParams();
    //let params = useParams();
    const {slug} = match.params;

   useEffect(() => {
    loadProduct()
    loadCategories()
   },[])

   const loadProduct = () => {
       getProduct(slug)
       .then((res) => {
           //console.log('Single Product',res)
           //1 load single product
           setValues({...values,...res.data})

           //2 load single product category subs
           getCategorySubcategories(res.data.category._id)
           .then((res) => {
            setSubCategoryOptions(res.data) // on first load, show defaults subs
           });

           //3 prepare array of subcategories ids to show as default sub values in antd Select

           let array = []
           res.data.subcategories.map((s) =>{
               array.push(s._id)
           });

           console.log("ARRAY: ",array);
           setArrayOfSubcategoriesId((previous) => array); //require for ant design select to work

       });
      
   }


   const loadCategories = () => {
       getCategories()
       .then((res) => {
           //console.log("SHOW CATGEORIES DATA",res.data)
            //setValues({...values,categories: res.data})
            setCategories(res.data)
       }).catch((err) => {
           console.log(err.response.data)
       })
   }

   const handleCategoryChange = (e) => {

    e.preventDefault()

    console.log("CLICKED CATEGORY: ",e.target.value)

    setValues({...values,subcategories:[]})

    setSelectedCategory(e.target.value);

    getCategorySubcategories(e.target.value)
    .then((res) => {
        
        setSubCategoryOptions(res.data)
    })

    //setShowSubCategory(true)

    console.log("EXISTING CATEGORY values.category ", values.category);


    //if user clicks back to the original category
    //show its sub categories in default
    if(values.category._id === e.target.value)
    {
        loadProduct()
    }

    //clear old sub category ids
    setArrayOfSubcategoriesId([])
}



   const handleSubmit = (e) =>{
        e.preventDefault()

        setLoading(true)

        values.subcategories = arrayOfSubcategories
        values.category = selectedCategory ? selectedCategory : values.category

        updateProduct(slug,values,user.token)
        .then((res) => {

            setLoading(false)
            toast.success(` "${res.data.title}" se  actualizó correctamente`);

            history.push("/admin/products");

        }).catch((err) => {
            console.log(err)
            setLoading(false)
            toast.error(err.response.data.err)
        })

   }

   const handleChange = (e) => {
        setValues({...values,[e.target.name]: e.target.value})

        console.log(e.target.name," ----- ",e.target.value)
   }

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                   
                {loading ? <LoadingOutlined className="text-danger h2" /> :  <h4>Actualizar Producto</h4>}
               {/* {JSON.stringify(values)} */}
                <hr/>

                <div className="p-3">
                    <FileUploadForm values={values} setValues={setValues}  setLoading={setLoading} />
                </div>
               
                
                <ProductUpdateForm handleSubmit={handleSubmit} handleChange={handleChange} 
                 values={values}  setValues={setValues} handleCategoryChange={handleCategoryChange} 
                 categories={categories} subcategoryOptions={subcategoryOptions}
                  arrayOfSubcategories={arrayOfSubcategories} setArrayOfSubcategoriesId={setArrayOfSubcategoriesId}
                  selectedCategory={selectedCategory} />
                   

                </div>
            </div>  
        </div>
    )
}

export default ProductUpdate