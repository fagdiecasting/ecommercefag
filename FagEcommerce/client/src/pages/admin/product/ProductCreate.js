import React,{useState,useEffect} from "react";
import AdminNav from "../../../components/nav/AdminNav"
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {createProduct,getProducts,removeProduct} from "../../../functions/product"
import { Link } from "react-router-dom";
import {EditOutlined,DeleteOutlined} from '@ant-design/icons'
import ProductForm from "../../../components/forms/ProductForm";
import LocalSearch from "../../../components/forms/LocalSearch";
import {getCategories,getCategorySubcategories} from "../../../functions/category"
import {getSubCategories} from '../../../functions/subcategory'
import {getColor, getColors} from "../../../functions/color"
import FileUploadForm from "../../../components/forms/FileUploadForm";
import {LoadingOutlined} from '@ant-design/icons'
import axios from "axios";


const initialState = {
    title:'',
    description:'',
    price:'',
    categories:[],
    category:'',
    subcategories:[],
    shipping:'',
    quantity:'',
    images:[],
    //colors:["Gris","Plateado","Dorado","Verde","Blanco","Negro","Rojo","Azul","Azul Obscuro"],
    //colors:[],
    colors:["Color Base"],
    brands:["Marca 1","Marca 2","Marca 3","Marca 4"],
    color:'',
    brand:''
}


const ProductCreate = () => {

    const [name,setName] = useState("")
    const [loading,setLoading] = useState(false)
    const [subcategoryOptions,setSubCategoryOptions] = useState([])
    const [showSubCategory,setShowSubCategory] = useState(false)
    const [values,setValues] = useState(initialState)
    //const [colors,setColors] = useState([])


    useEffect(() => {
        //loadAllProducts()
        loadCategories()
        //loadSubcategories()
        /*getSubCategories()
        .then((res) => {
            console.log("SUBCATEGORIES LOAD IN EFFECT",res.data)
        })*/
       
        //loadColors()
        //loadColors()


    },[])

    //const [colors,setColors] = useState("")

    //destructure
    const {title,description,price,categories,category,subcategories,shipping,quantity,images,brands,color,colors,brand} = values

    const {user} = useSelector(state =>( {...state}))


    //searching filtering
    const [keyword,setKeyword] = useState("")

    




    const loadCategories = () => {
        getCategories()
        .then((res) => {
            setValues({...values,categories:res.data})
        }).catch((err) => {
            toast.error(`Hubo un error al cargar las categorías existentes`)
            toast.error(err.response.data)
        })

        
    }

    

    const loadColors = () => {

        getColors()
        .then((res) => {
            setValues({...values,colors:res.data})
            //setColors(res.data)
            console.log("COLORS ON PRODUCTS",res.data)
        }).catch((err) =>{
            toast.error(err.response.data)
        })

        
        
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true)

        createProduct(values,user.token)
        .then((res) => {
            console.log(res)
            setLoading(false)
            
            window.location.reload()

            toast.success(`El producto ${res.data.title} se creó existosamente`)
           
        }).catch((err) =>{
            console.log(err)
            setLoading(false)
            /*if(err.response.status === 400)
            {
                toast.error(`El producto ${title} no se pudo crear `)
                toast.error(err.response.data)
            }*/

            toast.error(`El producto ${title} no se pudo crear `)
            toast.error(err.response.data.err)
        
        })

    }

    const handleChange = (e) => {
        setValues({...values,[e.target.name]: e.target.value})

        console.log(e.target.name," ----- ",e.target.value)
    }

    const handleColorChange = (e) =>{

        e.preventDefault()

        console.log("CLICKED COLOR",e.target.value)

        setValues({...values,colors: e.target.value})

        //setColors({colors:e.target.value})


        
    } 

    const handleCategoryChange = (e) => {

        e.preventDefault()

        console.log("CLICKED CATEGORY",e.target.value)

        setValues({...values,subcategories:[],category: e.target.value})


        getCategorySubcategories(e.target.value)
        .then((res) => {
            console.log("SUB OPTIONS",res)
            setSubCategoryOptions(res.data)
        })

        setShowSubCategory(true)
    }



    //setp 4
    const searched = (keyword) => (c) => c.name.toLowerCase().includes(keyword)
   

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                    {loading ? <LoadingOutlined className="text-danger h2" /> :  <h4>Crear Producto</h4>}
                     <br/>
                {/*<ProductForm handleSubmit={handleSubmit} handleChange={handleChange} title={title} description={description}
                price={price} shipping={shipping} quantity={quantity} color={colors} brand={brands} values={values} /> */}

                {/*{JSON.stringify(categories)}*/}

                {/*{JSON.stringify(values.subcategories)}*/}

                {/*{JSON.stringify(values.images)}*/}

                <div className="p-3">
                    <FileUploadForm values={values} setValues={setValues}  setLoading={setLoading} />
                </div>
                

                <ProductForm handleSubmit={handleSubmit} handleChange={handleChange} 
                 values={values} handleCategoryChange={handleCategoryChange}  handleColorChange={handleColorChange}
                 subcategoryOptions={subcategoryOptions} showSubCategory={showSubCategory} setValues={setValues} />

        
            
                    {/*Step 2 and Step 3*/}
                    <br/>
                    <LocalSearch keyword={keyword} setKeyword={setKeyword} />


                     <hr/>
                     {/*{categories.length}
                     {JSON.stringify(categories)}*/}

                     {/*Step 5*/}
                   

                </div>
            </div>  
        </div>
    )
}

export default ProductCreate