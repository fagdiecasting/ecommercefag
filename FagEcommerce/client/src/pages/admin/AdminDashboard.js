import React,{useState,useEffect} from "react";
import AdminNav from "../../components/nav/AdminNav"
import {getOrders,changeOrderStatus} from "../../functions/admin"
import {useSelector} from 'react-redux'
import {toast} from 'react-toastify'
import Orders from "../../components/order/Orders"

const AdminDashboard = () => {    

    const [orders,setOrders] = useState([])
    const {user} = useSelector((state) => ({...state}));


    useEffect(() => {
        loadOrders()
    },[])

    const loadOrders = () =>{
        getOrders(user.token)
        .then((res) => {
            setOrders(res.data)
            toast.success(`Pedidos obtenidos correctamente`)
            console.log(JSON.stringify(res.data,null,4))
        })
        .catch((err) =>{

            console.log("GET ORDERS FAILED --->",err)
            toast.error(`Hubo un Error para ontener los pedidos`)
        })
    }


    const handleStatusChange = (orderId,orderStatus) =>{
        changeOrderStatus(orderId,orderStatus,user.token)
        .then((res) =>{
            toast.success(`El estatus del pedido se cambio exitosamente`);
            loadOrders()
        })
        .catch((err) =>{
            console.log("CHANHE ORDER STATUS FAILED ---> ",err)
            toast.error("No se pudo actualizar el estatus del pedido");
        })
    }

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                    
                    <h5>Dashboard Administrador</h5>
                  
                    {/*JSON.stringify(orders)*/}
                  
                    <Orders orders={orders} handleStatusChange={handleStatusChange} />

                </div>
            </div>  
        </div>
    )
}

export default AdminDashboard