import React,{useState,useEffect} from 'react'
import {useSelector,useDispatch} from 'react-redux'
import {toast} from 'react-toastify'
import DatePicker from 'react-datepicker'
import { createCoupon,getCoupons,removeCoupon } from '../../../functions/Coupon'
import {DeleteOutlined, DeletOutlined,LoadingOutlined} from '@ant-design/icons'
import AdminNav from "../../../components/nav/AdminNav"


const CreateCouponPage = () => {

    const [name,setName] = useState('')
    const [expiry,setExpiry] = useState('')
    const [discount,setDiscount] = useState('')
    const [loading,setLoading] = useState(false)
    const [coupons,setCoupons] = useState([])


    const {user} = useSelector((state) => ({...state}))

    const dispatch = useDispatch()

    useEffect(() => {
       loadCoupons()
    },[])

    const loadCoupons = () => {
        getCoupons()
        .then((res) => {
            setCoupons(res.data)
            console.log("LENGTH COPUNS ---->",res.data)
        })
    }


    const handleSubmit = (e) =>{

        e.preventDefault()

        setLoading(true)

        console.table(name,expiry,discount)

        createCoupon({name,expiry,discount},user.token)
        .then((res) =>{
            setLoading(false)

            setName('')
            setDiscount('')
            setExpiry('')
            loadCoupons()
            toast.success(`El cupón ${res.data.name} se ha creado`)
        }).catch((err) =>{
            toast.error(`El cupón ${name} no se ha creado --> ${err}`)
        })

    }

    const handleRemove = (couponId) =>{

        if(window.confirm('Borrar Cupón?'))
        {
            setLoading(true)
            removeCoupon(couponId,user.token)
            .then((res) => {
                loadCoupons()
                setLoading(false)
                toast.success(`${res.data.name} se ha eliminado correctamente`)
            }).catch((err) =>{
                toast.success(`${name} no se pudo eliminar :( ${err}`)
            })
        }
       
    }


    return(
        <>
            <div className='container-fluid'>
                <div className='row'>

                    <div className='col-md-2'>
                    <AdminNav />
                    </div>
                    

                    <div className='col-md-10'>
                        

                        {loading ? (<LoadingOutlined className='text-danger' />) : (<h4>Cupón</h4>) }

                        <form onSubmit={handleSubmit}>
                            <div className='form-group'>
                                <h6>Nombre</h6>
                                <input type="text" className='form-control' value={name} onChange={(e) => setName(e.target.value)} autoFocus required />
                            </div>
                            <br/>
                            <div className='form-group'>
                                <h6>Descuento %</h6>
                                <input type="number" placeholder='Ingresa el  porcentaje a descontar (1%,10%,70%,etc...)' className='form-control' value={discount} onChange={(e) => setDiscount(e.target.value)} required />
                            </div>
                            <br/>
                            <div className='form-group'>
                                <h6>Fecha de Expiración</h6>
                                <DatePicker className='form-control' selected={expiry} value={expiry} required onChange={(date) => setExpiry(date)}  />
                            </div>

                            <br/>
                            <button className='btn btn-outline-primary'>Guardar</button>
                        </form>

                        <br/>

                       <h4>Existen {coupons.length} Cupones</h4>

                        <table className='table table-bordered'>
                            <thead className='thead-light'>
                                <tr>
                                    <th scope='col' >Nombre</th>
                                    <th scope='col' >Descuento</th>
                                    <th scope='col' >Fecha de Expiración </th>
                                    <th scope='col' >Acción</th>
                                </tr>
                            </thead>

                            <tbody>
                                {coupons.map((c) => (
                                    <tr key={c._id}>
                                        <td>{c.name}</td>
                                        <td>{c.discount} %</td>
                                        <td>{new Date(c.expiry).toLocaleDateString()}</td>
                                        
                                        {/*<td>{c.expiry.split("T")[0]}</td>*/}
                                        <td className='text-center'>
                                            <DeleteOutlined onClick={() => handleRemove(c._id)} className='text-danger pointer' />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>

                        </table>


                   </div>
                </div>
                
            </div>
        </>
    )

}

export default CreateCouponPage