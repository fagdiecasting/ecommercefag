import React,{useState,useEffect} from "react";
import AdminNav from "../../../components/nav/AdminNav"
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {getCategory,updateCategory} from "../../../functions/category";
import CategoryForm from "../../../components/forms/CategoryForm";


const CategoryUpdate = ({history,match}) => {

    const [name,setName] = useState("")
    const [loading,setLoading] = useState(false)
   

    const {user} = useSelector(state =>( {...state}))

  

    useEffect(() => {
        loadCategory()
    },[])

    const loadCategory = () => {
        getCategory(match.params.slug)
        .then((res) => {
            setName(res.data.name)
        }).catch((err) => {
            toast.error(`Hubo un error al actualizar la categoría`)
            toast.error(err.response.data)
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true)

        updateCategory(match.params.slug,{name},user.token)
        .then((res) => {
            setLoading(false)
            setName('')
            toast.success(`La categoría ${res.data.name} se actualizó exitosamente`)

            history.push("/admin/category")
          
        }).catch((err) => {
            setLoading(false)
            toast.error(`la categoría ${name} ya existe`)
            if(err.response.status === 400)
            {
               
                toast.error(err.response.data)
            }
            
        })

    }

   

    

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                    {loading ? <h4 className="text-danger">Cargando...</h4> :  <h4>Editar Categoría</h4>}
                     <br/>
                     <CategoryForm handleSubmit={handleSubmit} name={name} setName={setName} />
                     
                    
                   

                </div>
            </div>  
        </div>
    )
}

export default CategoryUpdate
