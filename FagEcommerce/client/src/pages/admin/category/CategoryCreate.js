import React,{useState,useEffect} from "react";
import AdminNav from "../../../components/nav/AdminNav"
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {createCategory,getCategories,removeCategory} from "../../../functions/category"
import { Link } from "react-router-dom";
import {EditOutlined,DeleteOutlined} from '@ant-design/icons'
import CategoryForm from "../../../components/forms/CategoryForm";
import LocalSearch from "../../../components/forms/LocalSearch";


const CategoryCreate = () => {

    const [name,setName] = useState("")
    const [loading,setLoading] = useState(false)
    const [categories,setCategories] = useState([])
    

    const {user} = useSelector(state =>( {...state}))

    //searching filtering
    const [keyword,setKeyword] = useState("")

    useEffect(() => {
        loadCategories()
    },[])

    const loadCategories = () => {
        getCategories()
        .then((res) => {
            setCategories(res.data)
        }).catch((err) => {
            toast.error(`Hubo un error al cargar las categorías existentes`)
            toast.error(err.response.data)
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true)

        createCategory({name},user.token)
        .then((res) => {
            setLoading(false)
            setName('')
            toast.success(`La categoría ${res.data.name} se creó exitosamente`)
            loadCategories()
        }).catch((err) => {
            setLoading(false)
            if(err.response.status === 400)
            {
                toast.error(`la categoría ${name} ya existe`)
                toast.error(err.response.data)
            }
            
        })

    }

    const handleRemove = async(slug) => {
        if( window.confirm(`Borrar la categoría ${slug} ?`))
        {
            setLoading(true)
            removeCategory(slug,user.token)
            .then((res) =>{
                setLoading(false)
                toast.success(`La categoría ${res.data.name} se eliminó correctamente`)
                loadCategories()
            }).catch((err) => {
                setLoading(false)
                toast.error(`La categoría ${slug} no se pudo eliminar`)
                if(err.response.status === 400)
                {   
                    setLoading(false)
                    toast.error(err.response.data)
                }
            })
        }
    }

    

    //setp 4
    const searched = (keyword) => (c) => c.name.toLowerCase().includes(keyword)
   

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <AdminNav />
                </div>

                <div className='col'>
                    {loading ? <h4 className="text-danger">Cargando...</h4> :  <h4>Crear Categoría</h4>}
                     <br/>
                     <CategoryForm handleSubmit={handleSubmit} name={name} setName={setName} />

                    {/*Step 2 and Step 3*/}
                    <br/>
                    <LocalSearch keyword={keyword} setKeyword={setKeyword} />


                     <hr/>
                     {/*{categories.length}
                     {JSON.stringify(categories)}*/}

                     {/*Step 5*/}
                    {categories.filter(searched(keyword)).map((c) => (
                        <div className="alert alert-secondary" key={c._id}>
                            {c.name}
                            <span onClick={() => handleRemove(c.slug)} className="btn btn-lg "><DeleteOutlined className="text-danger"  /></span>
                            <Link to={`/admin/category/${c.slug}`}> <span className="btn btn-lg "><EditOutlined className="text-primary" /></span>{" "}</Link>
                        </div>
                    ))}

                </div>
            </div>  
        </div>
    )
}

export default CategoryCreate