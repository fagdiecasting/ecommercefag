import React,{useState,useEffect} from 'react'
import {LoadingOutlined} from '@ant-design/icons'
import { getCategory } from '../../functions/category'
import ProductCard from '../../components/cards/ProductCard'
import {Link} from 'react-router-dom'


const CategoryHome = ({match}) => {

    const [category,setCategory] = useState({})
    const [products,setProducts] = useState([])
    const [loading,setLoading] = useState(false)

    const {slug} = match.params

    useEffect(() => {
        setLoading(true)
        getCategory(slug)
        .then((res) => {
           
            setCategory(res.data.category)
            setProducts(res.data.products)
            console.log(JSON.stringify(res.data,null,4))

            setLoading(false)
        })
    },[])

    return(
        <>
           <div className='container-fluid'>
                <div className='row'>
                    <div className='col'>
                        {loading ? <div style={{backgroundColor:'#DED8D8'}}><LoadingOutlined className='text-danger text-center p-3 mt-5 mb-5 display-4 ' /> </div> 
                        : ( <h3 className='text-dark text-center p-3 mt-5 mb-5 display-4 ' style={{backgroundColor:'#DED8D8'}}>
                                {products.length} Productos en la Categoría "{category.name}"
                             </h3>)}
                    </div>
                </div>

            <div className='row'>
                {products.map((p) => (
                    <div key={p._id} className='col-md-4'>
                        <ProductCard product={p} />
                    </div>
                ))}
            </div>


           </div>
        </>
    )

}

export default CategoryHome