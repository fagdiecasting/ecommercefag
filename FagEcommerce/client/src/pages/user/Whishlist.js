import React,{useState,useEffect} from 'react'
import UserNav from '../../components/nav/UserNav'
import {getWhishlist,removeWhishlist} from '../../functions/user'
import {useSelector,useDispatch } from 'react-redux'
import {Link} from 'react-router-dom'
import {DeleteOutlined} from '@ant-design/icons'

const Whishlist = () => {

    const [whishlist,setWhishlist] = useState([])

    const {user} = useSelector((state) => ({...state}))

    const dispatch = useDispatch()



    useEffect(() => {
        loadWhishlist()
    },[])

    const loadWhishlist = () =>{
        getWhishlist(user.token)
        .then((res) =>{
            //console.log("GET WHISHLIST -----",res.data)
            setWhishlist(res.data.whishlist)
        })
    }

    const handleRemove = (productId) => {

        removeWhishlist(productId,user.token)
        .then((res) =>{
            loadWhishlist()
        })
    }

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <UserNav />
                </div>

                <div className='col'>
                    <h4>Lista de Deseos</h4>
                    {whishlist.map((p) => (
                        <div key={p._id} className="alert alert-secondary">
                            <Link to={`/product/${p.slug}`}>{p.title}</Link>
                            <span onClick={() => handleRemove(p._id)} className='btn btn-sm float-right'>
                                <DeleteOutlined className='text-danger' />
                            </span>
                        </div>    
                    ))}
                </div>
            </div>  
        </div>
    )
}

export  default Whishlist