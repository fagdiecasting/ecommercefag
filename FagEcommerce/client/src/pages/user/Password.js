import React,{useState} from 'react'
import UserNav from '../../components/nav/UserNav'
import {auth} from '../../firebase'
import {toast} from 'react-toastify'



const Password = () => {

    const [password,setPassword] = useState("")
    const [loading,setLoading] = useState(false)


    const handleSubmit =  async (e) => {
        e.preventDefault()
        setLoading(true)

        await auth.currentUser.updatePassword(password)
        .then((res) => {
            setLoading(false);
            toast.success(`La Contraseña se cambio exitosamente`)

        }).catch((err) => {
            setLoading(false)
            toast.error(`No se pudo cambiar la Contraseña`)
        });

    }

    const passwordUpdateForm  =  () => {

        return(
        <form onSubmit={handleSubmit}>
            <div className='form-group'>
                <label>Tu Contraseña</label>
                <br/>
                <br/>
                <input type="password" onChange={(e) => setPassword(e.target.value)} className='form-control'
                placeholder='Ingresa tu Nueva Contraseña ' disabled={loading} value={password} />
                <br/>
                <button className='btn btn-primary' disabled={!password || password.length < 6 || loading} >Cambiar</button>
            </div>
        </form>
        )

    }

    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <UserNav />
                </div>

                <div className='col'>
                   {loading ? <h4 className='text-danger'>Cargando...</h4> :   <h4>Cambiar Contraseña</h4>}
                    <br/>
                    {passwordUpdateForm()}
                </div>
            </div>  
        </div>
    )
}

export  default Password