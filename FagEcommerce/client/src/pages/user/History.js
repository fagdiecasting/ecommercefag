import React,{useState,useEffect} from 'react'
import UserNav from '../../components/nav/UserNav'
import {getUserOrders} from '../../functions/user'
import {useSelector,useDispatch} from 'react-redux'
import {CheckCircleOutlined,CloseCircleOutlined} from '@ant-design/icons'
import {toast} from 'react-toastify'
import ShowPaymentInfo from '../../components/cards/ShowPaymentInfo'
import { PDFDownloadLink } from '@react-pdf/renderer';
import Invoice from '../../components/order/Invoice'

const History = () => {

    const [orders,setOrders] = useState([])
    const {user} = useSelector((state) => ({...state}))

    useEffect(() => {

        loadUserEffect()

    },[])

    const loadUserEffect = () =>{

        getUserOrders(user.token)
        .then((res) =>{
            console.log(JSON.stringify(res.data),null,4)
            setOrders(res.data)
        })

    }

    const showOrderInTable = (order) => (
       <table className='table table-bordered'>
           <thead className='thead-light'>
                <tr>
                    <th scope='col'>Nombre</th>
                    <th scope='col'>Precio</th>
                    <th scope='col'>Marca</th>
                    <th scope='col'>Color</th>
                    <th scope='col'>Cantidad</th>
                    <th scope='col'>Envio</th>
                </tr>
           </thead>
           <tbody>
               {order.products.map((p,i) => (
                   <tr key={i}>
                       <td>{p.product.title}</td>
                       <td>{p.product.price}</td>
                       <td>{p.product.brand}</td>
                       <td>{p.color}</td>
                       <td>{p.count}</td>
                       <td>{p.product.shipping === 'Si' ? <CheckCircleOutlined style={{color:'green'}} /> :
                        <CloseCircleOutlined style={{color:'red'}} /> }</td>
                   </tr>
               ))}
           </tbody>
       </table>
    )


    const showDowloadLink = (order) => (
        /*<PDFDownloadLink document={
            <Document>
                <Page size="A4">
                    <View>
                        <Text>Section #1</Text>
                        <Text>Section #2</Text>
                    </View>
                </Page>
            </Document>
        }
        
        fileName='invoice.pdf'
        className="btn btn-sm btn-block btn-outline-primary"

        >
            Descargar PDF
        </PDFDownloadLink>*/


        <PDFDownloadLink document={
           <Invoice order={order} />
        }
        
        fileName='invoice.pdf'
        className="btn btn-sm btn-block btn-outline-primary"

        >
            Descargar PDF
        </PDFDownloadLink>


    )


    const showEachOrders = () => {
        return(
            <>
            {orders.reverse().map((order,i) => (
                <div key={i} className="m-5 card p-3">
                    <ShowPaymentInfo order={order} />
                    {showOrderInTable(order)}

                    <div className='row'>
                       <div className='col'>
                            {showDowloadLink(order)}
                       </div>
                    </div>

                </div>
            ))}
            </>
        )
    }

    
    return(
        <div className='container-fluid'>
            <div className='row'>
               
                <div className='col-md-2'>
                    <UserNav />
                </div>

                <div className='col text-center'>
                    <h5>Historial de Compras</h5>
                    <br/>

                   
                    {orders.length > 0 ? `${user.name} ha comprado estos productos` : "No hay productos comprados"}
                  
                    <br/>

                    {showEachOrders()}
                    

                </div>

                
                
               
            </div>  

           
        </div>
    )
}

export  default History