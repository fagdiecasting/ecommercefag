import React,{useState,useEffect} from "react";
import {getProductsByCount,fetchProductsByFilter} from '../functions/product'
import {getCategories} from "../functions/category"
import {getSubCategories} from '../functions/subcategory'
import { useSelector,useDispatch } from "react-redux";
import ProductCard from '../components/cards/ProductCard'
import { LoadingOutlined,DollarOutlined,DownSquareOutlined,StarOutlined,LayoutOutlined,BgColorsOutlined,CarOutlined  } from "@ant-design/icons";
import {Slider,Menu,Checkbox,Radio} from 'antd'
import StarForm from "../components/forms/StarForm"


const Shop = () => {

    const [products,setProducts] = useState([])
    const [loading,setLoading] = useState(false)
    const [price,setPrice] = useState([0, 0])
    const [ok,setOk] = useState(false)
    const [categories,setCategories] = useState([])
    const [categoryIds,setCategoryIds] = useState([])
    const [star,setStar] = useState('')

    //subcategories to filter
    const [subcategories,setSubCategories] = useState([])
    const [subcategory,setSubcategory] = useState('')

    //brands to filter
    const [brands,setBrands] = useState(["Marca 1","Marca 2","Marca 3","Marca 4"])
    const [brand,setBrand] = useState('')


    //colors to filter
    //const [colors,setColors] = useState(["Gris","Plateado","Dorado","Verde","Blanco","Negro","Rojo","Azul","Azul Obscuro"])
    const [colors,setColors] = useState(["Color Base"])
    const [color,setColor] = useState('')


    //shipping option to filter
    const [shipping,setShipping] = useState('')

    let dispatch = useDispatch()

    let {search} = useSelector((state) => ({...state}))

    const {text} = search


    const {SubMenu} = Menu



    useEffect(() => {
        loadAllProducts()
        loadCategories()
        loadSubcategories()
    },[])


    
    const fetchProducts = (arg) => {
        fetchProductsByFilter(arg)
        .then((res) => {
            setProducts(res.data)
        })
    }

    const loadCategories = () => {

        getCategories()
        .then((res) => {
            setCategories(res.data)
        })
    }

    const loadSubcategories = () =>{

        getSubCategories()
        .then((res)=>{
            setSubCategories(res.data)
        })
    }


    //1- load all products on page by default 
    const loadAllProducts  = () => {

        //setLoading(true)

        getProductsByCount(12)
        .then((p) => {
            setProducts(p.data)
            setLoading(false)
        })

        
    }
    

    //2 load products on user search input
    useEffect(() => {

        
        const delayed = setTimeout(() => {
            fetchProducts({query: text})

            if(!text)
            {
                loadAllProducts()
            }
            
        },300)

        setPrice([0, 0])
        setCategoryIds([])
        setStar("")
        setSubcategory("")
        setBrand('')
        setColor('')
        setShipping('')

        return () => clearTimeout(delayed)

    },[text])

   
    //3- load products bases on price range
    useEffect(() => {
        console.log("Ok to Request")

        fetchProductsByFilter({price: price})

        /*if(price === 0)
        {
            loadAllProducts()
        }*/

    },[ok]);


    const handleSlider = (value) => {
        dispatch({
          type: "SEARCH_QUERY",
          payload: { text: "" },
        });

        setCategoryIds([])

        setPrice(value);
        setStar("")
        setSubcategory("")
        setBrand('')

        setColor('')

        setShipping('')

        setTimeout(() => {
          setOk(!ok);
        }, 300);
      };



    //4- Load Products based on Categor
    //show categories in a list of  checkbox

    const showCategories = () =>  categories.map((c) => (
                <div key={c._id} >
                    <Checkbox onChange={handleCheck} className="pb-2 pl-4 pr-4 pt-1 " value={c._id} name="category" checked={categoryIds.includes(c._id)} >
                        {c.name}
                    </Checkbox>
                    <br/>
                </div>
            ))
        
    
    //handle check for categories
    const handleCheck = (e) =>{

        dispatch({
            type:"SEARCH_QUERY",
            payload:{text:""}
        });

        setPrice([0, 0])
        setStar("")
        setSubcategory("")
        setBrand('')
        setColor('')
        setShipping('')
        
        //console.log(e.target.value)
        let inTheState = [...categoryIds];
        let justChecked = e.target.value
        let foundInTheState = inTheState.indexOf(justChecked) // index or -1

        //indexOf method if not found return -1 else return index
        if(foundInTheState === -1)
        {
            inTheState.push(justChecked)
        }else{
            //if found pull out one item from index
            inTheState.splice(foundInTheState,1);
        }


        setCategoryIds(inTheState)

        //console.log(inTheState)

        //this function is for get the products based on categories,price,or searching query,and so on with fetchProductsByFilter function
        fetchProducts({category: inTheState})

        if(!inTheState.length)
        {
            loadAllProducts()
        }

    };


    //5 show products by star rating

    const handleStarClick = (num) => {
        console.log("STAR RATING SEARCH ---> ",num)
        dispatch({
            type:"SEARCH_QUERY",
            payload:{text:""}

        })

        setPrice([0, 0])
        setCategoryIds([])
        setStar(num)
        setSubcategory("")
        setBrand('')
        setColor('')
        setShipping('')
        fetchProducts({stars: num});

        /*if(num === 0)
        {
            loadAllProducts()
        }*/

    }

    const showStars = () =>{
        return(
            <>
            <div className="pr-4 pl-4 pb-2">
                <StarForm starClick={handleStarClick} numberOfStars={5} />
                <StarForm starClick={handleStarClick} numberOfStars={4} />
                <StarForm starClick={handleStarClick} numberOfStars={3} />
                <StarForm starClick={handleStarClick} numberOfStars={2} />
                <StarForm starClick={handleStarClick} numberOfStars={1} />
            </div>
            </>
        )
    }


     //6- Load Products based on SubCategories

    const showSubCategories = () =>  subcategories.map((s) => (
        <div key={s._id} onClick={() => handleSubcategory(s)} className="p-1 m-1 badge bg-secondary" style={{cursor: "pointer"}} >
            
        {s.name}
    
        </div>
    ))


    const handleSubcategory = (sub) => {
        //console.log("SUBCATGORIES FILTERED",sub)

        setSubcategory(sub)

        dispatch({
            type:"SEARCH_QUERY",
            payload:{text:""}

        })

        setPrice([0, 0])
        setCategoryIds([])
        setStar('')
        setBrand('')
        setColor('')
        setShipping('')

        fetchProducts({subcategories: sub});

    }


    //7 - showproducts based on barnds name
    const showBrands = () => brands.map((b) => (
        <div>
            <Radio key={b} value={b}name={b} checked={b === brand} onChange={handleBrand} className="pl-4 pr-4 pb-1">
                {b}
            </Radio>
        </div>
    ))

    const handleBrand  = (e) =>{
        
    
        dispatch({
            type:"SEARCH_QUERY",
            payload:{text:""}

        })

        setPrice([0, 0])
        setCategoryIds([])
        setStar('')
        setSubcategory('')
        setColor('')

        setShipping('')

        setBrand(e.target.value)

        fetchProducts({brand: e.target.value})
    }


    //8-load cproducts based on colors
    const showColors = () => colors.map((c) => (
        <div>

            <Radio key={c} value={c} name={c} checked={c === color} onChange={hanldeColor} className="pl-4 pr-4 pb-1">
                {c}
            </Radio>

        </div>
    ))

    const hanldeColor = (e) =>{

        dispatch({
            type:"SEARCH_QUERY",
            payload:{text:""}

        })

        setPrice([0, 0])
        setCategoryIds([])
        setStar('')
        setSubcategory('')
        setBrand('')
        setShipping('')
        setColor(e.target.value)

        fetchProducts({color: e.target.value})

    }

    //9- load products based on shipping option

    const showShipping = () => (
        <>
            <Checkbox className="pb-2 pl-4 pr-4" onChange={handleShippingChange} value="Si" checked={shipping === 'Si'}>Si</Checkbox>

            <Checkbox className="pb-2 pl-4 pr-4" onChange={handleShippingChange} value="No" checked={shipping === 'No'}>No</Checkbox>
        </>
    )

    const handleShippingChange = (e) => {
        dispatch({
            type:"SEARCH_QUERY",
            payload:{text:""}

        })

        setPrice([0, 0])
        setCategoryIds([])
        setStar('')
        setSubcategory('')
        setBrand('')
        setColor('')
        setShipping(e.target.value)

        fetchProducts({shipping: e.target.value})

    }



    return(
        <>

            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-3">
                        <h4>Menú Buscar / Filtrar</h4>

                        <hr/>

                        <Menu defaultOpenKeys={["1", "2","3","4","5","6","7"]} mode="inline">
                            <SubMenu
                              key="1"
                              title={
                                            <span className="h6">
                                  <DollarOutlined /> Price
                                </span>
                              }
                            >
                            
                                <Slider
                                  className="ml-4 mr-4"
                                  tipFormatter={(v) => `$${v}`}
                                  range
                                  value={price}
                                  onChange={handleSlider}
                                  max="4999"
                                />

                            </SubMenu>
                            <hr/>
                            <SubMenu
                              key="2"
                              title={
                                    <span className="h6">
                                  <DownSquareOutlined /> Categorías
                                </span>
                              }
                            >
                            
                                {showCategories()}
                           

                            </SubMenu>

                              {/*Star Rating Filter*/}

                              <hr/>

                              <SubMenu
                              key="3"
                              title={
                                    <span className="h6">
                                  <StarOutlined /> Valoración
                                </span>
                              }
                            >
                            
                                {showStars()}
                           

                            </SubMenu>

                            {/*SubCategories Filter*/}

                            <hr/>
                            <SubMenu
                              key="4"
                              title={
                                    <span className="h6">
                                  <DownSquareOutlined /> Sub Categorías
                                </span>
                              }
                            >
                            
                                {showSubCategories()}
                           

                            </SubMenu>


                            {/*Brands Filter*/}

                            <hr/>
                            <SubMenu
                              key="5"
                              title={
                                    <span className="h6">
                                  <LayoutOutlined /> Marcas
                                </span>
                              }
                            >
                            
                                {showBrands()}
                           

                            </SubMenu>

                            {/*Colors Filter*/}

                            <hr/>
                            <SubMenu
                              key="6"
                              title={
                                    <span className="h6">
                                  <BgColorsOutlined /> Colores
                                </span>
                              }
                            >
                            
                                {showColors()}
                           

                            </SubMenu>

                             {/*Shipping Filter*/}

                             <hr/>
                            <SubMenu
                              key="7"
                              title={
                                    <span className="h6">
                                  <CarOutlined  /> Envío
                                </span>
                              }
                            >
                            
                                {showShipping()}
                           

                            </SubMenu>

                       </Menu>

                        
                    </div>

                    <div className="col-md-9 pt-3">
                       {loading ? (<LoadingOutlined className="text-danger" />) : (
                           <h4 className="">
                            Productos
                            </h4>
                       ) }


                       {products.length < 1 && <p>No se Encontraron Productos...</p>}

                        <div className="row pb-5">
                            {products.map((p) => (
                                <div key={p._id} className="col-md-4 mt-3">
                                    <ProductCard product={p} />
                                </div>    
                            ))}
                        </div>

                    </div>
                </div>
            </div>

        </>
    )

}
export default Shop