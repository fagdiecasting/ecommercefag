import React,{useState} from 'react'
import {
    AppstoreOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    PieChartOutlined,
    DesktopOutlined,
    ContainerOutlined,
    MailOutlined,
  } from '@ant-design/icons';
import { Menu, Button } from 'antd';

import Jumbotron from '../components/cards/Jumbotron';

import NewArrivals from '../components/home/NewArrivals'

import BestSellers from '../components/home/BestSellers'

import CategoryList from "../components/category/CategoryList"

import SubCategoryList from "../components/subcategory/SubCategoryList"

import { Link } from 'react-router-dom';  

const {SubMenu,Item} = Menu  



const Home = () => {

    const [collapsed,setCollapsed] = useState(false)
    const [products,setProducts] = useState([])
    const [loading,setLoading] = useState(false)

    const toggleCollapsed = () => {
        
        //setCollapsed(true)

       setCollapsed(!collapsed);
    }

    return(
      <>
        <div className='p-5 text-danger font-weight-bold h1 text-center' style={{backgroundColor:'#DED8D8'}}>

          {loading ? (<h4>Cargando...</h4>) : <Jumbotron text={['Todos los Productos','Nuevos Productos','Lo más Vendido']} />}
                  
        </div>

        <h4 className='text-center p-3 mt-4 mb-5 display-5' style={{backgroundColor:'#DED8D8'}}>
          Nuevos Productos
        </h4>
         
        <NewArrivals />

        <h4 className='text-center p-3 mt-4 mb-5 display-5' style={{backgroundColor:'#DED8D8'}}>
          Lo más Vendido
        </h4>
         
        <BestSellers />

        <br/>
        <br/>

        <h4 className='text-center p-3 mt-4 mb-5 display-5' style={{backgroundColor:'#DED8D8'}}>
          Categorías
        </h4>

        <CategoryList />

        <br/>
        <br/>

        <h4 className='text-center p-3 mt-4 mb-5 display-5' style={{backgroundColor:'#DED8D8'}}>
          Sub Categorías
        </h4>

        <SubCategoryList />
    
    </>
        
    )
}

export default Home;