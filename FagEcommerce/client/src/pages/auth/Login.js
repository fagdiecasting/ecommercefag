import React,{useState,useEffect} from 'react';
import {auth, googleAuthProvider} from '../../firebase';
import {toast} from 'react-toastify';
import { Input } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { Button } from 'antd';
import { MailOutlined,GoogleOutlined,FacebookOutlined,WindowsOutlined } from '@ant-design/icons';
import { useDispatch , useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import {createOrUpdateUser} from '../../functions/auth'

const {Password} = Input;




const Login = ({history}) => {    

    const [email,setEmail] = useState("sgfarrera115@hotmail.com")
    const [password,setPassword] = useState("Muukalainen115")
    const [loading,setLoading] = useState(false)

    const {user} = useSelector((state) => ({...state}))

    let intended = history.location.state

    useEffect(() => {

        

        if(intended)
        {
            return;
        }else{

            if(user && user.token)
            {
                history.push('/')
            }
        }

        
    },[user,history]) //esto permite evitar ir a rutas no autorizadas cuando estoy logeado

   let dispatch = useDispatch()


   const roleBasedRedirect = (res) => {

        //check if intended
        //let intended = history.location.state

        if(intended)
        {
            history.push(intended.from)

        }else{

            if(res.data.role == 'admin')
            {
                history.push('/admin/dashboard')
            }else {
                history.push('/user/history')
            }
        }

        
    }

    const handleSubmit = async (e) =>{
        e.preventDefault()

        setLoading(true)
        //console.table(email,password)

        try{
           const result = await auth.signInWithEmailAndPassword(email,password)

            //console.log(result)

            const {user} = result
            const idTokenResult = await user.getIdTokenResult();


            createOrUpdateUser(idTokenResult.token)
            .then((res) =>{
                dispatch({
                    type:"LOGGED_IN_USER",
                    payload:{
                        name: res.data.name,
                        email: res.data.email,
                        token: idTokenResult.token,
                        role: res.data.role,
                        _id: res.data._id
                    }
                });

                roleBasedRedirect(res)
            }).catch((err) =>{
                console.log(err)
            })

            /*dispatch({
                type:"LOGGED_IN_USER",
                payload:{
                    email:user.email,
                    token: idTokenResult.token
                }
            });*/

            //history.push("/");

        }catch(error)
        {
            //console.log(error)
            toast.error(`El correo o la contraseña son incorrectos o el correo aun no esta registrado`)
            setLoading(false)
        }

    }

    const googleLogin = async () => {
        auth.signInWithPopup(googleAuthProvider)
        .then(async (result) => {
            const {user} = result
            const idTokenResult = await user.getIdTokenResult();

            
            createOrUpdateUser(idTokenResult.token)
            .then((res) =>{
                dispatch({
                    type:"LOGGED_IN_USER",
                    payload:{
                        name: res.data.name,
                        email: res.data.email,
                        token: idTokenResult.token,
                        role: res.data.role,
                        _id: res.data._id
                    }
                });

                roleBasedRedirect(res)
                
            }).catch((err) =>{
                console.log(err)
            })

            /*dispatch({
                type:"LOGGED_IN_USER",
                payload:{
                    email:user.email,
                    token: idTokenResult.token
                }
            }); */

            //history.push("/")

            

        })
        .catch((err) => {
            console.log(err)

            toast.error(err.message)
        })
    }


    const loginForm = () => <form onSubmit={handleSubmit}>
        
        <input type="email" className='form-control' placeholder='Ingresa tu correo... ej.(xxx@gmail.com)' 
        value={email} onChange={e => setEmail(e.target.value)} autoFocus />
    
        
        <br/>


        <div className='form-group'>
            <Password
                placeholder="Ingresa tu contraseña"
                value={password}
                iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                onChange={e => setPassword(e.target.value)}
            />
        </div>
           
        <br/>
        <Button icon={<MailOutlined />} onClick={handleSubmit} type='light' 
        className='mb-3' block shape='round' size='large' disabled={!email || password.length < 6}>
            inicia sesión con tu correo y contraseña
        </Button>

    </form>



    return(
        <div className='container p-5'>
            <div className='row'> {/*We Have 12 columns*/}
                <div className='col-md-6 offset-md-3'>

                    {loading ? <h4 className='text-danger'>Cargando...</h4> : <h4>Iniciar Sesión</h4>} 
                
                   {loginForm()}


                   <Button icon={<GoogleOutlined />} onClick={googleLogin} type='danger' 
                    className='mb-3' block shape='round' size='large'>
                    inicia sesión con google Gmail
                    </Button>

                    <Button icon={<FacebookOutlined />} onClick={handleSubmit} type='primary' 
                    className='mb-3' block shape='round' size='large' >
                    inicia sesión con Facebook
                    </Button>

                    <Button icon={<WindowsOutlined />} onClick={handleSubmit} style={{backgroundColor:'#0F2153',color:'#FEFEFE'}}
                    className='mb-3' block shape='round' size='large' disabled={!email || password.length < 6}>
                    inicia sesión con Microsoft
                    </Button>

                    <Link to="/forgot/password" style={{fontSize:17,color:'red'}}>
                        Olvide mi contraseña ?
                    </Link>

                </div>
            </div>
        </div>
    )
    
}

export default Login;