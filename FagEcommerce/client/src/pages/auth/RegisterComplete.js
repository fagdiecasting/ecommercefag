import React,{useState,useEffect} from 'react';
import {auth} from '../../firebase';
import {toast} from 'react-toastify';
import { Input } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { useDispatch , useSelector} from 'react-redux';
import {createOrUpdateUser} from '../../functions/auth'

const {Password} = Input;




const RegisterComplete = ({history}) => {    

    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")

    //props.history or {history}

    const {user} = useSelector((state) => ({...state}))

    

    useEffect(() => {
        setEmail(window.localStorage.getItem("emailForRegistration"))
        //console.log(window.location.href)
        if(user && user.token)
        {
            history.push('/')
        }
    },[user,history])


    let dispatch = useDispatch()

    const handleSubmit = async (e) =>{
        e.preventDefault()

        //validation
        if(!email || !password)
        {
            toast.error(`El email y contraseña son obligatorios`)
            return;
        }

        if(password.length < 6)
        {
            toast.error(`La contraseña debe contener por lo menos 6 caracteres `)
            return;
        }

        
        try{

            const result = await auth.signInWithEmailLink(email,window.location.href)
            //console.log('RESUKT',result);
            if(result.user.emailVerified)
            {
                //remove the email from local storage
                window.localStorage.removeItem("emailForRegistration");

                //get user id token
                let user = auth.currentUser;
                await user.updatePassword(password)
                const idTokenResult = await user.getIdTokenResult()
                //redux store

                //console.log('user: ', user , ' idtoken: ',idTokenResult)



                createOrUpdateUser(idTokenResult.token)
                .then((res) =>{
                    dispatch({
                        type:"LOGGED_IN_USER",
                        payload:{
                            name: res.data.name,
                            email: res.data.email,
                            token: idTokenResult.token,
                            role: res.data.role,
                            _id: res.data._id
                        }
                    })
                }).catch((err) =>{
                    console.log(err)
                })



                //redirect
                history.push('/')

                toast.success(`Bienvenido ${user.email}`)

            }
        }catch(error)
        {
            console.log(error)
            toast.error(error.message)
        }
    }

    const completeRegistrationForm = () => <form onSubmit={handleSubmit}>

        <input type="email" className='form-control' value={email} disabled />
        <br/>
        <Password
            placeholder="Ingresa tu contraseña"
            value={password}
            iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
            autoFocus
            onChange={e => setPassword(e.target.value)}
        />
        <br/>
        <br/>
        <button type='submit' className='btn btn-success'>Completar Registro</button>

    </form>

    return(
        <div className='container p-5'>
            <div className='row'> {/*We Have 12 columns*/}
                <div className='col-md-6 offset-md-3'>
                    <h4>
                        Completar Registro
                    </h4>
                   {completeRegistrationForm()}
                </div>
            </div>
        </div>
    )
    
}

export default RegisterComplete;