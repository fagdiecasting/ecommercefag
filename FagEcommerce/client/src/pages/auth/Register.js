import React,{useState,useEffect} from 'react';
import {auth} from '../../firebase';
import {toast} from 'react-toastify';
import { useSelector } from 'react-redux';

const Register = ({history}) => {    

    const [email,setEmail] = useState("")

    const {user} = useSelector((state) => ({...state}))

    useEffect(() => {
        if(user && user.token)
        {
            history.push('/')
        }
    },[user,history])



    const handleSubmit = async (e) =>{
        e.preventDefault()

        if(!email)
        {
            toast.error(`El email es obligatorio`)
            return;
        }

        const config ={
            url: process.env.REACT_APP_REGISTER_REDIRECT_URL,
            handleCodeInApp: true
        }

        await auth.sendSignInLinkToEmail(email,config)
        toast.success(`Se ha enviado un email de confirmacion a ${email},porfavor de clic en el link para completar el registro`);
        toast.success(`Ya puedes cerrar esta ventana`);

        //save the email in the local storage
        window.localStorage.setItem('emailForRegistration',email)

        //clear state
        setEmail('')

    }

    const registerForm = () => <form onSubmit={handleSubmit}>

        <input type="email" className='form-control' placeholder='Ingresa tu correo... ej.(xxx@gmail.com)' 
        value={email} onChange={e => setEmail(e.target.value)} autoFocus />
        <br/>
        <button type='submit' className='btn btn-success'>Registrar</button>

    </form>

    return(
        <div className='container p-5'>
            <div className='row'> {/*We Have 12 columns*/}
                <div className='col-md-6 offset-md-3'>
                    <h4>
                        Registro
                    </h4>
                   {registerForm()}
                </div>
            </div>
        </div>
    )
    
}

export default Register;