import React,{useState,useEffect} from 'react';
import {auth} from '../../firebase';
import {toast} from 'react-toastify';
import { Input } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { Button } from 'antd';
import { useSelector } from 'react-redux';


const {Password} = Input;

const ForgotPassword = ({history}) => {

    const [email,setEmail] = useState("")
    const [loading,setLoading] = useState(false)

    const {user} = useSelector((state) => ({...state}))

    useEffect(() => {
        if(user && user.token)
        {
            history.push('/')
        }
    },[user])

    const handleSubmit = async (e) => {
        e.preventDefault()

        setLoading(true)

        const config ={
            url: process.env.REACT_APP_FORGOT_PASSWORD_REDIRECT,
            handleCodeInApp: true
        }

        await auth.sendPasswordResetEmail(email,config)
        .then(() => {
            setEmail('')
            setLoading(false)
            toast.success(`Te hemos enviado un enlace a tu correo para restablecer tu contraseña,Ya puedes cerrar este ventana`)
        })
        .catch((error) => {
            setLoading(false)
            //toast.error(error.message)
            toast.error(`Este correo no esta registrado`)
        })

    }

    return <div className='container col-md-6 offset-md-3 p-5'>

            {loading ? <h4 className='text-danger'>Cargando...</h4> : <h4>Cambiar Contraseña</h4>}

            <form onSubmit={handleSubmit}>
                <input type="email" className='form-control' value={email} onChange={(e) => setEmail(e.target.value)} 
                placeholder='Ingresa tu correo electronico' autoFocus/>

                <br/>

                <button className='btn btn-success' disabled={!email}>
                    Enviar
                </button>
            </form>

    </div>

}

export default ForgotPassword;