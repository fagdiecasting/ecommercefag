import React,{useState} from "react";
import { useSelector,useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import ProductCardInCheckout from "../components/cards/ProductCardInCheckout"
import {userCart} from '../functions/user'

const Cart = ({history}) => {

    const {user,cart} = useSelector((state) => ({...state}))

    const dispatch = useDispatch()

    const getTotal = () =>{
        return cart.reduce((currentValue,nextValue) => {
            return currentValue + nextValue.count * nextValue.price
        },0)
    }


    const saveOrderToDb = () => {
    
        //alert("SAVE ORRDER TO DB")

        //console.log("cart",JSON.stringify(cart, null, 4))

        userCart(cart,user.token)
        .then((res) => {
            console.log("CART PSOR RESPONSE",res)

            if(res.data.ok)
            {
                history.push("/checkout")
            }
        }).catch((err) => {
            console.log("CART SAVE ERROR",err)
        })

        
    }

    //save chash on db
    const saveCashOrderToDb = () =>{

        dispatch({
            type:"COD",
            payload: true,
        })

        userCart(cart,user.token)
        .then((res) => {
            console.log("COD RESPONSE",res)

            if(res.data.ok)
            {
                history.push("/checkout")
            }
        }).catch((err) => {
            console.log("CART SAVE ERROR",err)
        })

    }

    const showCartItems = () => (
        
        <table className="table table-bordered">
            <thead className="thead-light">
                <tr>
                    <th scope="col">Imagen</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Color</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Envío</th>
                    <th scope="col">Eliminar</th>
                </tr>
            </thead>


        {cart.map((p) => (
           <ProductCardInCheckout key={p._id} p={p} />
        ))}

        </table>
    )

    return(
        <>
            <div className="container-fluid pt-2">
   

                <div className="row">
                <h4>Carrito / {cart.length} productos</h4>
                    <div className="col-md-8">
                        {!cart.length ? <h4>No hay productos en el carrito. <Link to="/shop"> Continua Comprando </Link> </h4> : (showCartItems())}
                    </div>
                    <div className="col-md-4">
                        <h4>Pedidos</h4>
                        <hr/>
                        <p>Productos</p>
                        {cart.map((c,index) => (
                            <div key={index}>
                                <p>{c.title} x {c.count} = ${c.price * c.count} MXN</p>
                            </div>
                        ))}
                        <hr/>
                        Total: <b>${getTotal()} MXN</b>
                        <hr/>
                        {
                            user ? (
                               <>
                                <button onClick={saveOrderToDb} className="btn btn-sm btn-primary mt-2" disabled={!cart.length} >Proceder a comprar</button>
                                <br/>
                                <button onClick={saveCashOrderToDb} className="btn btn-sm btn-warning mt-2" disabled={!cart.length} >Pagar en efectivo</button>
                               </>
                            ) : (
                                <button className="btn btn-sm btn-primary mt-2"> <Link to={{
                                    pathname:'/login',
                                    state:{from:'cart'},
                                }} style={{color:'white'}} >Identifícate para comprar</Link> </button>
                            )
                        }
                    </div>
                </div>

            </div>
        </>
    )

}


export default Cart