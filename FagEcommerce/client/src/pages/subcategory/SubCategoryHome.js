import React,{useState,useEffect} from 'react'
import { LoadingOutlined } from '@ant-design/icons'
import ProductCard from '../../components/cards/ProductCard'
import { getSubCategory } from '../../functions/subcategory'


const SubCategoryHome = ({match}) => {

    const [loading,setLoading] = useState(false)
    const [subcategory,setSubCategory] = useState({})
    const [categories,setCategories] = useState([])
    const [products,setProducts] = useState([])

    const {slug} = match.params


    useEffect(() => {

        setLoading(true)

        getSubCategory(slug)
        .then((res) => {

            setSubCategory(res.data.subcategory)
            setProducts(res.data.products)

            console.log(JSON.stringify(res.data,null,4))

            setLoading(false)
        })

    },[])

    return(
        <>


            <div className='container-fluid'>
                <div className='row'>
                    <div className='col'>
                    {loading ? <div style={{backgroundColor:'#DED8D8'}}><LoadingOutlined className='text-danger text-center p-3 mt-5 mb-5 display-4 ' /> </div> 
                        : ( <h3 className='text-dark text-center p-3 mt-5 mb-5 display-4 ' style={{backgroundColor:'#DED8D8'}}>
                                {products.length} Productos en la Sub Categoría "{subcategory.name}"
                             </h3>)}
                    </div>
                </div>
            </div>


            <div className='container'>
                <div className='row'>
                    {products.map((p) => (
                        <div key={p._id} className='col-md-4'> 

                                <ProductCard product={p} />

                        </div>
                       
                    ))}
                    
                </div>
            </div>
        </>
    )
}


export default SubCategoryHome