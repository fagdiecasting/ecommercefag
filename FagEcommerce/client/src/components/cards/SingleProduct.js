import React,{useState} from "react";
import { Card, Tabs,Input,Tooltip} from "antd";
import { Link } from "react-router-dom";
import {HeartOutlined,ShoppingCartOutlined,EyeOutlined} from '@ant-design/icons'
import machine from '../../images/maquina2.png'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import ProductListItems from "./ProductListItems"
import StarRatings from 'react-star-ratings'
import ReactDisqusComments from 'react-disqus-comments'
import {DiscussionEmbed, CommentCount,Recommendations,CommentEmbed } from 'disqus-react'
import RatingModal from '../modal/RatingModal'
import CommentsComponent from '../comments/CommentsComponent'
import {showAverage} from "../../functions/rating"
import _ from 'lodash'
import { useSelector,useDispatch } from "react-redux";
import {addToWhishlist} from "../../functions/user"
import {toast} from 'react-toastify'
import {useHistory} from 'react-router-dom'

const {TabPane} = Tabs

const {TextArea } = Input


//this is children component of Product Page
const SingleProduct = ({product,onStarClick,star}) => {

    const {title,images,description,_id,slug} = product
    const [tooltip,setTooltip] = useState('Clic para Agregar')

    const {user} = useSelector((state) => ({...state}))
    const dispatch = useDispatch()

    const history = useHistory()

    const handleAddToCart = () =>{

        

        //create cart array
        let cart = []
        if(typeof window !== 'undefined')
        {
            //if cart is in local storage GET it
            if(localStorage.getItem('cart'))
            {
                cart = JSON.parse(localStorage.getItem('cart'))
            }

            //push new product to cart array
            cart.push({
                ...product,
                count: 1
            });

            //remove duplicates
            let unique = _.uniqWith(cart,_.isEqual)

            //save to local storage
            console.log("UNIQUE",unique)
            localStorage.setItem('cart',JSON.stringify(unique))

            //showTooltip
            setTooltip("Agregado")

            //add to redux state
            dispatch({
                type:'ADD_TO_CART',
                payload: unique
            })

        }
    }

    const handleAddToWhishlist = (e) =>{

        e.preventDefault()
        addToWhishlist(product._id,user.token)
        .then((res) => {

            console.log("ADD TO WHISHLIST ----------")
            console.log(JSON.stringify(res.data),null,4)
            toast.success("Agregado a la lista de Deseos")
            history.push("/user/whishlist")

        }).catch((err) =>{
            console.log("ADD TO WHISHLIST ERROR --> ",err)
            toast.error("No se pudo agregar a tu Lista de Deseos")
        })

    }

    return(
        <>
            
            <div className="col-md-7">
                { images && images.length ? <Carousel showArrows={true} autoPlay infiniteLoop>
                    {images && images.map((i) => <img src={i.url} key={i.public_id} />)}
                </Carousel> : 

                    <Card 
                     cover={<img src={machine}  className="mb-3 card-image" />}     
                    >

                    </Card>
                
                }

                <Tabs type="card">
                    <TabPane tab="Descripción" key="1">
                        {description && description}
                    </TabPane>
                    <TabPane tab="Mas" key="2">
                        Llamanos al numero XXXXXXXXXXX para saber mas acerca de este producto.
                        fddfdfdfdf
                    </TabPane>
                </Tabs>

                <CommentsComponent>

                    <DiscussionEmbed
                        shortname='fagdiecasting'
                        config={
                            {
                                url: `http://localhost:3000/product/${slug}`,
                                identifier: slug,
                                title: "",
                                language: 'es_MX' //e.g. for Traditional Chinese (Taiwan)	
                            }
                        }
    
                    />

                </CommentsComponent>


            </div>

            <div className="col-md-5">
            <h1 className="p-3 text-center" style={{backgroundColor:'#DED8D8'}}>{title}</h1>

            {product && product.ratings && product.ratings.length > 0 ? showAverage(product)
                         : <div className="text-center pt-1 pb-3"> Aún sin Calificación </div> }

            <Card    
            cover={<img src={images && images.length ? images[0].url : machine} 
            style={{height:'150px',objectFit:"scale-down"}} className="p-1" />} 
            actions={
                [   
                    <a onClick={handleAddToWhishlist}>
                        <HeartOutlined className="text-info" /> <br/> Añadir a Lista de Deseos
                    </a>,
                    
                    <>
                    <Tooltip title={tooltip}>
                        <a onClick={handleAddToCart}>
                        <ShoppingCartOutlined  className="text-danger" /> <br/> Agregar al Carrito
                        </a>
                    </Tooltip>
                    </> ,
                    
                    <RatingModal>
                
                    <StarRatings
                        name={_id}
                        numberOfStars={5}
                        rating={star}
                        isSelectable={true}
                        starRatedColor="red"
                        changeRating={onStarClick}
                    />
                    <br/>
                    <br/>
                        <TextArea rows={4}  />
                        
                    </RatingModal>,
                    
                ]
            }
            >

                
              <ProductListItems product={product} />
            </Card>

            </div>
        </>
    )
}

export default SingleProduct