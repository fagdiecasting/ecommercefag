import React, { useState } from "react";
import { Card,Tooltip} from "antd";
import { EyeOutlined,ShoppingCartOutlined } from "@ant-design/icons";
import machine from '../../images/maquina2.png'
import { Link } from "react-router-dom";
import {showAverage} from "../../functions/rating"
import _ from 'lodash'
import { useSelector,useDispatch } from "react-redux";

const {Meta} = Card

const ProductCard = ({product}) => {

    const [tooltip,setTooltip] = useState('Clic para Agregar')

    const {images,title,description,slug,price} = product


    const {user} = useSelector((state) => ({...state}))
    const dispatch = useDispatch()


    const handleAddToCart = () =>{

        

        //create cart array
        let cart = []
        if(typeof window !== 'undefined')
        {
            //if cart is in local storage GET it
            if(localStorage.getItem('cart'))
            {
                cart = JSON.parse(localStorage.getItem('cart'))
            }

            //push new product to cart array
            cart.push({
                ...product,
                count: 1
            });

            //remove duplicates
            let unique = _.uniqWith(cart,_.isEqual)

            //save to local storage
            console.log("UNIQUE",unique)
            localStorage.setItem('cart',JSON.stringify(unique))

            //showTooltip
            setTooltip("Agregado")

            //add to redux state
            dispatch({
                type:'ADD_TO_CART',
                payload: unique
            })

            //show cart items in side drawer
            dispatch({
                type:"SET_VISIBLE",
                payload: true
            })

        }
    }

    return(
        <>

        {product && product.ratings && product.ratings.length > 0 ? showAverage(product)
                         : <div className="text-center pt-1 pb-3"> Aún sin Calificación </div> }

        <Card    
            cover={<img src={images && images.length ? images[0].url : machine} 
            style={{height:'150px',objectFit:"scale-down"}} className="p-1" />} 
            actions={
                [   
                    <Link to={`/product/${slug}`}>
                        <EyeOutlined className="text-primary" /> <br/> Ver Producto
                    </Link>,
                    <>
                    <Tooltip title={tooltip}>
                        <a  onClick={handleAddToCart} >
                        <ShoppingCartOutlined  className="text-danger" /> <br/> 
                        {product.quantity < 1 ? 'No hay stock de este producto' : 'Agregar al Carrito' }
                        </a>
                    </Tooltip>
                    </>                  
                    
                ]
            }
        >
            <Meta title={`${title} - $${price} MXN`} description={`${description && description.substring(0,45)}...`} />
        </Card>
        </>
    )
}


export default ProductCard