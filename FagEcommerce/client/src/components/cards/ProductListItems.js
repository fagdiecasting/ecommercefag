import React from "react";
import { Link } from "react-router-dom";

const ProductListItems = ({product}) => {

    const {price,category,subcategories,shipping,color,brand,quantity,sold} = product

    return(
        <>
            <ul className="list-group">
                <li className="list-group-item">
                    Precio{" "} <span className="label label-default label-pill float-end">$ {price} MXN</span>
                </li>
                {category && <li className="list-group-item">
                    Categoría{" "} <Link to={`/category/${category.slug}`} className="label label-default label-pill float-end">{category.name}</Link>
                </li>}

                {subcategories && (

                    <li className="list-group-item">
                        sub
                        {subcategories.map((s) =>(
                             <Link 
                             key={s._id}
                             to={`/subcategory/${s.slug}`}
                             className="label label-default label-pill float-end"
                              >
                             {s.name}
                         </Link>
                        ))}
                    </li>
                   
                )}


                <li className="list-group-item">
                    Envío{" "} <span className="label label-default label-pill float-end"> {shipping} </span>
                </li>

                <li className="list-group-item">
                    Color{" "} <span className="label label-default label-pill float-end"> {color} </span>
                </li>

                <li className="list-group-item">
                    Marca{" "} <span className="label label-default label-pill float-end"> {brand} </span>
                </li>

                <li className="list-group-item">
                    Cantidad Disponible{" "} <span className="label label-default label-pill float-end"> {quantity} </span>
                </li>

                <li className="list-group-item">
                    Vendidos{" "} <span className="label label-default label-pill float-end"> {sold} </span>
                </li>
                
            </ul>
        </>
    )
}

export default ProductListItems