import React from "react";
import ModalImage from 'react-modal-image'
import machine from '../../images/maquina2.png'
import { useDispatch } from "react-redux";
import {toast} from 'react-toastify'
import {CheckCircleOutlined,CheckOutlined,CloseCircleOutlined,CloseOutlined} from '@ant-design/icons'

const ProductCardInCheckout = ({p}) =>{

   //const colors = ["Gris","Plateado","Dorado","Verde","Blanco","Negro","Rojo","Azul","Azul Obscuro"]
   const colors = ["Color Base"]

   let dispatch = useDispatch()

   //update color in the local storage too
   const handleColorChange = (e) => {
     console.log("color changed to: ",e.target.value)

     let cart = []

     if(typeof window !== 'undefined')
     {
        if(localStorage.getItem('cart'))
        {
            cart = JSON.parse(localStorage.getItem("cart"))
        }

        cart.map((product,i) => {

            if(product._id === p._id)
            {
                cart[i].color = e.target.value
            }

        });

        localStorage.setItem('cart',JSON.stringify(cart))
        dispatch({
            type:'ADD_TO_CART',
            payload: cart,
        })
     }

   }


   //handle quantity value 
   const handleQuantityChange = (e) =>{

    //console.log('avaibale quantiry',p.quantity)

   

        let count = e.target.value < 1 ? 1 : e.target.value;

        if(count > p.quantity)
        {
            toast.error(`Cantidad maxima disponible es ${p.quantity}`)
            return;
        }

        let cart = []

        if(typeof window !== 'undefined')
        {
            if(localStorage.getItem('cart'))
            {
                cart = JSON.parse(localStorage.getItem('cart'))
            }

            cart.map((product,i) => {
                //cart[i].count = e.target.value

                if(product._id == p._id)
                {
                    cart[i].count = count
                }

            });

            localStorage.setItem('cart',JSON.stringify(cart))

            dispatch({
                type:"ADD_TO_CART",
                payload: cart
            })


        }

   }

   const handleRemove = () => {
       //console.log(p._id, "To remove")

       let cart = []

       if(typeof window !== "undefined")
       {
            if(localStorage.getItem('cart'))
            {
                cart  = JSON.parse(localStorage.getItem('cart'))
            }

            cart.map((product,i) => {
                //cart[i].count = e.target.value

                if(product._id == p._id)
                {
                    cart.splice(i,1)
                }

            });

            localStorage.setItem('cart',JSON.stringify(cart))

            dispatch({
                type:"ADD_TO_CART",
                payload: cart
            })
       }
   }

    return(
        <>
            <tbody>
                <tr>
                    <td>
                        <div style={{width:'100px',height:'auto'}} >
                            {p.images.length ? (<ModalImage  small={p.images[0].url} large={p.images[0].url} />) : 
                            (<ModalImage  small={machine} large={machine} />)}
                        </div>
                    </td>
                    <td>{p.title}</td>
                    <td>${p.price}</td>
                    <td>{p.brand}</td>
                    <td>
                        <select name="color"onChange={handleColorChange} className="form-control">
                            {p.color ? (<option value={p.color} >{p.color}</option>) : (<option>Selecciona...</option>)}

                            {colors.filter((c) => c !== p.color).map((c) => (
                                <option key={c} value={c} >
                                    {c}
                                </option>
                            ))}
                        </select>
                    </td>
                    <td className="text-center">
                        <input type="number" className="form-control" value={p.count} onChange={handleQuantityChange} />
                    </td>
                    <td className="text-center">
                        {p.shipping === "Si" ? <CheckOutlined className="text-success" /> : <CloseCircleOutlined className="text-danger" /> }
                    </td>
                    <td className="text-center">
                        <CloseOutlined onClick={handleRemove} className="text-danger pointer" />
                    </td>
                </tr>
            </tbody>
        </>
    )
}

export default ProductCardInCheckout

