import React from 'react'


const ShowPaymentInfo = ({order,showStatus = true}) => {
    return(
        <>
        <div>
            <p>
                <span>Orden Id: {order.paymentIntent.id}</span>{" / "}
                <span>Cantidad: {(order.paymentIntent.amount /= 100).toLocaleString('es-MX',{
                    style:'currency',
                    currency:"MXN"
                })}</span>{" / "}
                <span>Divisa: {order.paymentIntent.currency.toUpperCase()}</span>{" / "}
                <span>Método: {order.paymentIntent.payment_method_types[0] === "card" ? "Tarjeta" : "Efectivo"}</span>{" / "}
                <span>Pago: {order.paymentIntent.status === "succeeded" ? "exitoso".toUpperCase() : "efectivo en la entrega"}</span>{" / "}
                <span>Fecha de Compra:{" "} {new Date(order.paymentIntent.created * 1000).toLocaleString()}</span>{" / "}
                <br/>
               {showStatus && (<span className='badge bg-primary text-white'>Estatus: {order.orderStatus}</span>)}
            </p>
        </div>
        </>
    )
}

export default ShowPaymentInfo