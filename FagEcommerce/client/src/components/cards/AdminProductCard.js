import React from "react";
import { Card } from "antd";
import machine from '../../images/maquina2.png'
import { EditOutlined,DeleteOutlined } from "@ant-design/icons";
import {Link} from 'react-router-dom'

const {Meta} = Card
 
const AdminProductCard  = ({product,handleRemoveProduct}) => {

    //destructure
    const {title,description,images,slug} = product

    return(
        <>
        <Card 
            hoverable
            cover={<img src={images && images.length ? images[0].url : machine} 
            style={{height:'150px',objectFit:"scale-down"}} className="p-1" />}
            actions={
                [   
                    <Link to={`/admin/products/${slug}`}>
                        <EditOutlined className="text-primary" />,
                    </Link>,                  
                    <DeleteOutlined onClick={() => handleRemoveProduct(slug)} className="text-danger" />
                ]
            }
        >
            <Meta title={title} description={`${description && description.substring(0,45)}...`} />
        </Card>
        
        </>
    )
}

export default AdminProductCard