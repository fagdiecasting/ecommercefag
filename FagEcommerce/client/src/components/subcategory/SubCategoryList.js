import React,{useState,useEffect} from 'react'
import { Link } from 'react-router-dom'
import { LoadingOutlined, TrophyOutlined } from '@ant-design/icons'
import {getSubCategories} from "../../functions/subcategory"


const SubCategoryList = () => {

    const [loading,setLoading] = useState(false)
    const [subcategories,setSubcategories] = useState([])


    useEffect(() => {

        setLoading(TrophyOutlined)
        getSubCategories()
        .then((res) => {
            console.log("SUBCATEGORIES LIST",res.data)
            setSubcategories(res.data)
            setLoading(false)
        })

    },[])


    const showSubCategories = () => subcategories.map((s) => (
        <div key={s._id} className='col btn btn-outline-light btn-lg btn-block btn-raised m-3'>
            <Link to={`/subcategory/${s.slug}`}>
                {s.name}
            </Link>
        </div>
    ))
    return(
        <>
            
            <div className='container'>
                
                    <div className='row'>

                     {loading ? <LoadingOutlined className='text-danger' /> : showSubCategories()}

                    </div>
 
            </div>

            
        </>
    )
}

export default SubCategoryList