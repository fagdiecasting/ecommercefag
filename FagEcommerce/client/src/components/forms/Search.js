import React,{useState,useEffect} from 'react'
import {useSelector,useDispatch} from 'react-redux'
import {useHistory} from 'react-router-dom'
import { Input, Space } from 'antd';
import {SearchOutlined,AppstoreOutlined} from '@ant-design/icons'


//const { Search } = Input;

const Search = () =>{

    let dispatch = useDispatch()    
    const {search} = useSelector((state) => ({...state}))
    const {text} = search

    const history = useHistory()

    const handleChange = (e) => {
        dispatch({
            type:"SEARCH_QUERY",
            payload: {text: e.target.value},

        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        history.push(`/shop?${text}`)

        console.log("E ---->",e)
    }

    return(
        <>

            {/*<form className='form-inline my-4' onSubmit={handleSubmit}>
           
                <input onChange={handleChange} type="search" value={text} className='form-control' placeholder='Buscar...' /> 
                <SearchOutlined onClick={handleSubmit} style={{cursor:'pointer'}} />
                 
    </form>*/}

    <form className="form-inline my-1" onSubmit={handleSubmit}>
      
    
    <Input.Search placeholder="input search text" allowClear onChange={handleChange} onClick={handleSubmit} style={{ width: 200 }} />
      
    </form>
        </>
    )
}

export default Search