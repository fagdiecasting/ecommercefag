import React from "react";


const LocalSearch = ({keyword,setKeyword}) => {

        //step 3
const handleSearchChange = (e) => {
    e.preventDefault()

    setKeyword(e.target.value.toLowerCase())
}


        return(
            
            <div>
                {/*Step 2*/}
                <input type="search" value={keyword} placeholder="Filtro..." onChange={handleSearchChange} 
                className="form-control mb-4"  />
            </div>
        )
}

export default LocalSearch