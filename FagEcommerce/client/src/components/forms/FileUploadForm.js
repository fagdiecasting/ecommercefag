import React from "react"
import Resizer from 'react-image-file-resizer'
import axios from 'axios'
import { useSelector } from "react-redux"
import {Avatar,Badge} from 'antd'

const FileUploadForm = ({values,setValues,setLoading}) => {

    const {user} = useSelector((state) => ({...state}))

    const fileUploadAndResize = (e) => {
        //console.log(e.target.files)

        //resize
        let files = e.target.files

        let allUploadFiles = values.images

        if(files)
        {   

            setLoading(true)

            for(let i=0;i< files.length;i++)
            {
                Resizer.imageFileResizer(files[i],720,720,'PNG',100,0,(uri) => {
                    //console.log(uri)
                    axios.post(`${process.env.REACT_APP_API}/uploadimages`,{images: uri},{
                        headers:{
                            authtoken: user ? user.token : '',
                        }
                    })
                    .then((res) => {
                        console.log("IMAGE RESPONSE DATA",res)
                        setLoading(false)
                        allUploadFiles.push(res.data)

                        setValues({...values,images: allUploadFiles})
                    })
                    .catch((err) => {
                        setLoading(false)
                        console.log('CLOUDINARY ERROR',err)
                    })

                },"base64")
                
            }
        }

        //send back to server to upload to cloudinary

        //set url to images[] in the parent component - ProductCreate
    }

    const handleImageRemove = (id) => {
        setLoading(true)

        //console.log("remove image: ",id)

        axios.post( `${process.env.REACT_APP_API}/removeimage`,{public_id: id},{
            headers:{
                authtoken: user ? user.token : ""
            }
        }).then((res) => {

            setLoading(false)

            const {images} = values

            let filteredImages = images.filter((item) => {
                return item.public_id !== id
            });

            setValues({...values,images: filteredImages})
        }).catch((err) => {
            setLoading(false)
            console.log(err)
        })

    }

    return(

        <>
        
        <div>
            
            <label className="btn btn-primary" >Selecciona un archivo...
             <input type="file" multiple hidden accept="images/*" onChange={fileUploadAndResize} />
            </label>
            
        </div>
        <br/>
        <div >
        {values.images && values.images.map((image) => (
            <Badge count="X" key={image.public_id} 
            onClick={() => handleImageRemove(image.public_id)} style={{cursor:'pointer'}}  >
                <Avatar key={image.public_id} src={image.url} size={100} 
                shape="square" className="ml-3" />
            </Badge>
        ))} 
        </div>
        </>
       

    )
}

export default FileUploadForm