import React from "react";


const ColorForm  = (props) => {

    return(
        <>
            <form onSubmit={props.handleSubmit}>

                <div className="form-group">
                    <h6>Color</h6>
                    <input type="text" className="form-control" onChange={(e) => props.setName(e.target.value)}
                    value={props.name} placeholder="Ingresa el nombre del color..." />
                    <br/>
                    <button className="btn btn-success" disabled={!props.name || props.name.length < 3}>
                        Crear
                    </button>
                </div>

            </form>
        </>
    )
}

export default ColorForm