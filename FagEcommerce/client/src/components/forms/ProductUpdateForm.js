import React from 'react'
import {Select} from 'antd'

const {Option} = Select

const ProductUpdateForm = ({handleSubmit,handleChange,values,setValues,handleCategoryChange,subcategoryOptions,categories,
                            arrayOfSubcategories,setArrayOfSubcategoriesId, selectedCategory }) => {

    //destructure
    const {title,description,price,category,subcategories,
        shipping,quantity,images,colors,brands,color,brand} = values



    return(

        <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <h6>Nombre</h6>
                    <input type="text" name='title' className="form-control"  onChange={handleChange}  
                    value={title} placeholder="Ingresa el nombre del producto..." autoFocus required />
                    <br/>
`                   
                </div>

                <div className="form-group">
                    <h6>Descripción</h6>
                    <input type="text" name='description' className="form-control"  onChange={handleChange}  
                    value={description} placeholder="Ingresa la descripción del producto..."  required />
                    <br/>
`                   
                </div>

                <div className="form-group">
                    <h6>Precio</h6>
                    <input type="number" name='price' className="form-control"  onChange={handleChange}  
                    value={price} placeholder="Ingresa el precio del producto..."  required />
                    <br/>
`                   
                </div>

                
                <div className="form-group">
                    <h6>Envío</h6>
                    <select value={shipping === "Si" ? "Si" : "No"} name='shipping' className="form-select" onChange={handleChange}>
                        <option value="No">No</option>
                        <option value="Si">Si</option>
                    </select>
                    <br/>
`                   
                </div>

                <div className="form-group">
                    <h6>Cantidad</h6>
                    <input type="number" name='quantity' className="form-control"  onChange={handleChange}  
                    value={quantity} placeholder="Ingresa la cantidad del producto..."  />
                    <br/>
`                   
                </div>

                <div className="form-group">
                    <h6>Color</h6>
                    <select value={color} name='color' className="form-select" onChange={handleChange}>
                        {colors.map(c => <option key={c} value={c}>{c}</option>)}
                    </select>
                    <br/>
`                   
                </div>


                <div className="form-group">
                    <h6>Marca</h6>
                    <select value={brand} name='brand' className="form-select" onChange={handleChange}>
                        {brands.map(b => <option key={b} value={b}>{b}</option>)}
                    </select>
                    <br/>
`                   
                </div>

                <div className="form-group">
                        <h6>Categoría</h6>
                        
                        <select name="category" className="form-select" 
                        onChange={handleCategoryChange}
                         //value={category._id}
                         value={selectedCategory ? selectedCategory : category._id} >
                        
                           {categories.length > 0 && categories.map((c) => {
                                return(
                                    <option key={c._id} value={c._id}>{c.name}</option>
                                )
                           })}
                        </select>

                        <br/>
                </div>

                <div>
                   <h6>Sub Categoría</h6> 
                   <Select
                        mode="multiple"
                        style={{width:'100%'}}
                        placeholder="Selecciona una opcion..."
                        //value={subcategories}
                        value={arrayOfSubcategories}
                        //onChange={(value) => setValues({...values,subcategories:value})}
                        onChange={(value) => setArrayOfSubcategoriesId(value)}
                   >
                          {subcategoryOptions.length && subcategoryOptions.map((s) => 
                            (<Option key={s._id} value={s._id}>{s.name}</Option>))}
                           
                   </Select> 
                       
               </div>



              

               <br/> 

               <button  className='btn btn-success' disabled={!title || !description || !price}>
                        Actualizar
                </button>

                    
            </form>
    )
}

export default ProductUpdateForm