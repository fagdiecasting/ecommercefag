import React from 'react'

const SubCategoryForm = (props) => {
    return(

        <form onSubmit={props.handleSubmit}>
                <div className="form-group">
                    <h6>Nombre</h6>
                    <input type="text" className="form-control"  onChange={e => props.setName(e.target.value)}  
                    value={props.name} placeholder="Ingresa un nombre para la sub categoría..." autoFocus required  />
                    <br/>
`                   <button className='btn btn-success' disabled={!props.name || props.name.length < 3}>
                        Crear
                     </button>
                </div>
            </form>
    )
}

export default SubCategoryForm