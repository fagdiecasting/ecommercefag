import React,{useEffect, useState} from 'react'
import {Route} from 'react-router-dom'
import { useSelector } from 'react-redux'
import LoadingToRedirect from './LoadingToRedirect'
import {currentAdmin} from '../../functions/auth'
import { toast } from 'react-toastify'

const AdminRoute = ({children,...rest}) => {

    const {user} = useSelector((state) => ({...state}));
    const [ok,setOk] = useState(false)

    useEffect(() => {
        if(user && user.token)
        {
            currentAdmin(user.token)
            .then((res) => {
                console.log('CURRENT ADMIN RESPONSE',res)
                setOk(true)
            }).catch((err) =>{
                console.log(err)
                toast.dark(`Que te pasa? Tu no eres Administrador !!!`)
                setOk(false)
            })
        }
    },[user])

    return ok ? (
    <Route {...rest} /*render={() => children}*/  />
    ) : (
    <LoadingToRedirect />
    )
}

export default AdminRoute

