import React from 'react'
import { Document, Page, Text, View, StyleSheet,PDFViewer } from '@react-pdf/renderer';
import {Table,TableHeader,TableCell,TableBody,DataTableCell} from '@david.kucsai/react-pdf-table'

const Invoice = ({order}) =>{

    return(
        <>

        <Document>
            <Page style={styles.body}>
                <Text style={styles.header} fixed>~ {new Date().toLocaleString()} ~</Text>
                <Text style={styles.title}>Factura de Pedido</Text>
                <Text style={styles.author}>FAG Die Casting</Text>
                <Text style={styles.subtitle}>Pedido</Text>

                <Table>
                    <TableHeader>
                        <TableCell>Producto</TableCell>
                        <TableCell>Precio</TableCell>
                        <TableCell>Cantidad</TableCell>
                        <TableCell>Marca</TableCell>
                        <TableCell>Color</TableCell>
                    </TableHeader>
                </Table>

                <Table data={order.products}>
                    <TableBody>
                        <DataTableCell getContent={(n) => n.product.title} />
                        <DataTableCell getContent={(p) => `$${p.product.price} MXN`} />
                        <DataTableCell getContent={(c) => c.count} />
                        <DataTableCell getContent={(b) => b.product.brand} />
                        <DataTableCell getContent={(c) => c.color} />

                    </TableBody>
                </Table>


                <Text style={styles.text}>
                    <Text>Fecha: {"            "}{new Date(order.paymentIntent.created * 1000).toLocaleString()}</Text>
                    {"\n"}
                    <Text>ID del Pedido:{"          "}{order.paymentIntent.id}</Text>
                    {"\n"}
                    <Text>Estatus del Pedido:{"   "}{order.orderStatus}</Text>
                    {"\n"}
                    <Text>Pago Total:{"       "} ${order.paymentIntent.amount} MXN</Text>
                </Text>

                <Text style={styles.footer}>
                    Gracias por tu Compra
                </Text>
            </Page>
        </Document>
        
        </>
    )

}



const styles = StyleSheet.create({
    body: {
      paddingTop: 35,
      paddingBottom: 65,
      paddingHorizontal: 35,
    },
    title: {
      fontSize: 24,
      textAlign: "center",
    },
    author: {
      fontSize: 12,
      textAlign: "center",
      marginBottom: 40,
    },
    subtitle: {
      fontSize: 18,
      margin: 12,
    },
    text: {
      margin: 12,
      fontSize: 14,
      textAlign: "justify",
    },
    image: {
      marginVertical: 15,
      marginHorizontal: 100,
    },
    header: {
      fontSize: 12,
      marginBottom: 20,
      textAlign: "center",
      color: "grey",
    },
    footer: {
      padding: "100px",
      fontSize: 12,
      marginBottom: 20,
      textAlign: "center",
      color: "grey",
    },
    pageNumber: {
      position: "absolute",
      fontSize: 12,
      bottom: 30,
      left: 0,
      right: 0,
      textAlign: "center",
      color: "grey",
    },
  });

export default Invoice