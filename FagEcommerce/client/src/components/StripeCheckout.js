import React,{useState,useEffect} from "react";
import { CardElement,useStripe,useElements } from "@stripe/react-stripe-js";
import {useSelector,useDispatch} from 'react-redux'
import {createPaymentIntent} from "../functions/stripe"
import {Link} from 'react-router-dom'
import {Card} from 'antd'
import {DollarOutlined,CheckOutlined} from '@ant-design/icons'
import maquina from '../images/maquina2.png'
import {createOrder,emptyUserCart} from "../functions/user"

const StripeCheckout = ({history}) => {

    const {user,coupon} =useSelector((state) => ({...state}))
    const dispatch = useDispatch()

    const [succeeded,setSucceeded] = useState(false)
    const [error,setError] = useState(null)
    const [showprocessing,setShowProccessing] = useState('')
    const [disabled,setDisabled] = useState(true)
    const [clientSecret,setClientSecret] = useState('')

    const [cartTotal,setCartTotal] = useState(0)
    const [totalAfterDiscount,setTotalAfterDiscount] = useState(0)
    const [payable,setPayable] = useState(0)


    const stripe = useStripe()
    const elements = useElements()

    useEffect(() => {

        createPaymentIntent(user.token,coupon)
        .then((res) =>{
            console.log("CREATE PAYMENT INTENT -->>" ,res.data)
            setClientSecret(res.data.clientSecret)

            //set additional response on succesful payment
            setCartTotal(res.data.cartTotal)
            setTotalAfterDiscount(res.data.totalAfterDiscount)
            setPayable(res.data.payable)

        })


    },[]);

    const handleSubmit = async (e) => { 

        e.preventDefault()

        setShowProccessing(true)

        const payload = await stripe.confirmCardPayment(clientSecret,{
            payment_method:{
                card: elements.getElement(CardElement),
                billing_details: {
                    name: e.target.name.value
                }
            }
        });


        if(payload.error)
        {
            setError(`El Pago fallo ${payload.error.message}`)
            setShowProccessing(false)

        }else{
            //here get the result of the success payment
            //create order and save in database for admin process

            createOrder(payload,user.token)
            .then((res) =>{

                if(res.data.ok)
                {
                    //empty cart from local storage
                    if(typeof(window) !== 'undefined')
                    {
                        localStorage.removeItem("cart")
                    }

                    //empty cart from redux 
                    dispatch({
                        type:"ADD_TO_CART",
                        payload: [],
                    });

                    //reset coupon to false
                    dispatch({
                        type:"COUPON_APPLY",
                        payload: false,
                    });


                    //empty cart from database
                    emptyUserCart(user.token);
                }
            })

            //empty user cart from redux store and local storage
            console.log(JSON.stringify(payload,null,4))
            setError(null)
            setShowProccessing(false)
            setSucceeded(true)
        }

    }

    const handleChange = async (e) => { 

        //listen for changes in the card element
        //and display any errors as the customer types their card details
        setDisabled(e.empty) //diables pay button if errors
        setError(e.error ? e.error.message : ""); //show error message



    }

    const cardStyle = {
        style: {
          base: {
            color: "#32325d",
            fontFamily: "Arial, sans-serif",
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
              color: "#32325d",
            },
          },
          invalid: {
            color: "#fa755a",
            iconColor: "#fa755a",
          },
        },
      };

    return(
        <>
        {
            !succeeded && <div>
                {coupon && totalAfterDiscount !== undefined ? (<p className="alert alert-success">{`Total con descuento: $${totalAfterDiscount} MXN`}</p>) 
                : (<p className="alert alert-danger">No se aplico ningun descuento</p>)}
            </div>
        }

        <div className="text-center pb-5">
            <Card 
                cover={<img src={maquina} style={{height:'200px',width:'300px',objectFit:'cover',marginBottom:'-50px'}}  />}

                actions={[
                    <>
                        <DollarOutlined className="text-info" /> <br/> Total: $
                        {cartTotal} MXN
                    </>,
                    <>
                    <CheckOutlined className="text-info" /> <br/> Total a pagar: $
                    {(payable / 100).toFixed(2)} MXN
                    </>,
                ]}
            />
        </div>

        <form id="payment-form" className="stripe-form" onSubmit={handleSubmit}>
            <CardElement id="card-element" options={cardStyle} onChange={handleChange} />
            <button className="stripe-button" disabled={showprocessing || disabled || succeeded}>
                <span id="button-text">
                    {showprocessing ? (<div className="spinner" id="spinner">

                    </div>) : "Pagar"}
                </span>
            </button>
            <br/>
            {error && <div className="card-error" role="alert">{error}</div>}
            <br/>
            <p className={succeeded ? 'result-message' : 'result-message hidden'}> 
             Pago Exitoso. <Link to="/user/history"> Ve tu compra en tu historial.</Link>
            </p>
        </form>

        
        
        </>
    )

}


export default StripeCheckout