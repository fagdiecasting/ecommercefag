import React,{useReducer, useState} from 'react'
import { Menu,Badge} from 'antd';
import { AppstoreOutlined, SettingOutlined,UserOutlined,UserAddOutlined,LogoutOutlined,ShoppingOutlined,ShoppingCartOutlined } from '@ant-design/icons';
import { Image } from 'antd';
import {Link} from 'react-router-dom';
import firebase from '../../../node_modules/firebase/compat';
import { useDispatch,useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { auth } from '../../firebase';
import { useHistory } from 'react-router-dom';
import Search from '../forms/Search'
import "../../index.css"


const {SubMenu} = Menu; // Menu.SubMenu
const {Item} = Menu;

const Header = () => {
    const [current,setCurrent] = useState("home")

    let history = useHistory()
    let dispatch = useDispatch()
    let userName = auth.currentUser
    let {user,cart} = useSelector((state) => ({...state}))

    const handleClick = (e) =>{
        //console.log(e.key)
        setCurrent(e.key)
    }

    const logout = () => {

        //toast.dark(`Hasta luego ${userName.email}`)

        firebase.auth().signOut()
        
        dispatch({
            type:'LOGOUT',
            payload:null
        })

        history.push("/login")
        
    }

    return(

     
        <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal" style={{backgroundColor:'#C3C3C3'}}>
        




       
                        
        {/*<Item key="homeImage" style={{padding:5}}>

        <Link to="/">

            <Image
                width={50}
                height={50}
                src="https://res.cloudinary.com/fag-die-casting/image/upload/v1636395711/1636395709343.png"
            />
        </Link>


    </Item>*/}




        <Item key="home" icon={<AppstoreOutlined  />}  style={{color:'black'}} >
            <Link to="/" style={{color:'black'}}  >Inicio</Link>
        </Item>

        <Item key="shop" icon={<ShoppingOutlined  />}  style={{color:'black'}} >
            <Link to="/shop" style={{color:'black'}} >Tienda</Link>
        </Item>

        <Item key="cart" icon={<ShoppingCartOutlined   />}  style={{color:'black'}} className='me-auto' >
            <Link to="/cart" style={{color:'black'}} >
                <Badge count={cart.length} offset={[9, 0]} >
                    Carrito
                </Badge>
            </Link>
        </Item>


        <Item>
            <Search />
        </Item>
        
      



        {user && (

                
            <SubMenu key="SubMenu" icon={<SettingOutlined  />} title={user.email && user.email.split('@')[0]} style={{color:'black'}}>
                
                {user && user.role === 'subscriber' && (<Item><Link to="/user/history">Dashboard</Link></Item>)}

                {user && user.role === 'admin' && (<Item><Link to="/admin/dashboard">Dashboard</Link></Item>)}
                
                
                <Item key="logout" icon={<LogoutOutlined /> } onClick={logout} >Cerrar Sesión </Item>

            </SubMenu>

        )} 




        {!user && (

            <>

            <Item key="login" icon={<UserOutlined  />}  style={{color:'black'}}>
            <Link to="/login" style={{color:'black'}}>Login</Link>
            </Item>

            
            </>

        


        )}




    {!user && (



    <Item key="register" icon={<UserAddOutlined  />}  >

        
    <Link to="/register" style={{color:'black'}}>Registrar</Link>
    </Item>

    )}
       
        
      

    
       
        
           
    
       
        </Menu>

        


    )

}

export default Header;