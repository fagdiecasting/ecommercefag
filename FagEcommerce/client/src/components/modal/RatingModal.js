import React,{useState} from 'react'
import {Modal,Button,Input } from 'antd'
import {toast} from 'react-toastify'
import {useSelector} from 'react-redux'
import {StarOutlined} from '@ant-design/icons'
import {useHistory,useParams} from 'react-router-dom'

const {TextArea } = Input

const RatingModal = ({children}) => {

    const {user} = useSelector((state) => ({...state}));
    const [modalVisible,setModalVisible] = useState(false);

    let history = useHistory()
    //let params = useParams()
    let {slug} = useParams()

    console.log("PARAMS.SLUG",slug)

    const handleModal = () =>{
        if(user && user.token)
        {
            setModalVisible(true)
        }else{
            history.push({
                pathname:'/login',
                state: {from: `/product/${slug}`}
            });
        }
    }


    return(
        <>
            <div onClick={handleModal}>
                <StarOutlined className="text-danger"  /> <br/>
                {user ? "Deja tu Calificación" : "Logeate para dejar tu Calificación"}
            </div>
            
            <Modal title="Deja tu calificación" centered visible={modalVisible} 
            onOk={() => {
                setModalVisible(false) 
                toast.success("Gracias por tu calificación")
            }}

            onCancel={() => setModalVisible(false)}

            >
                {children}
            </Modal>
        </>
    )
}

export default RatingModal;