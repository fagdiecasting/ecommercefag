import React,{useState} from "react";
import {useSelector} from 'react-redux'
import {useHistory,useParams} from 'react-router-dom'
import {CommentOutlined} from '@ant-design/icons'

const CommentsComponent = ({children}) => {

    const [commentsVisible,setCommentsVisible] = useState(true)


    let history = useHistory()
    let {slug} = useParams()


    const {user} = useSelector((state) => ({...state}))

    const handleCommentsSection = () =>{

        if(user && user.token)
        {
            setCommentsVisible(true)
        }else{
            history.push({
                pathname:'/login',
                state:{from:`/product/${slug}`}
            })
        }


    }


    return(
        <>
            <section onClick={handleCommentsSection}>
                <CommentOutlined  />
                {user ? "Deja una reseña del producto" : "Logeate para ver los comentarios y puedas hacer una reseña"}
                {children}
            </section>
        </>
    )


}

export default CommentsComponent