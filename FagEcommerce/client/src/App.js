import logo from './logo.svg';
import './App.css';
import React,{useEffect} from 'react';
import {Switch,Route} from 'react-router-dom';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import "react-datepicker/dist/react-datepicker.css";

import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import Home from './pages/Home';
import RegisterComplete from './pages/auth/RegisterComplete';
import ForgotPassword from './pages/auth/ForgotPassword';
import History from "./pages/user/History";
import UserRoute from './components/routes/UserRoute';
import Password from '../src/pages/user/Password';
import Whishlist from '../src/pages/user/Whishlist';

//information pages
import AboutUs from './pages/information/AboutUs';
import Service from './pages/information/Service';


//Admin 
import AdminDashboard from './pages/admin/AdminDashboard';
import AdminRoute from './components/routes/AdminRoute';
import CategoryCreate from '../src/pages/admin/category/CategoryCreate';
import CategoryUpdate from './pages/admin/category/CategoryUpdate';
import SubCategoryCreate from './pages/admin/subcategory/SubCategoryCreate';
import SubCategoryUpdate from './pages/admin/subcategory/SubCategoryUpdate';
import ProductCreate from './pages/admin/product/ProductCreate';
import AllProducts from './pages/admin/product/AllProducts';
import ProductUpdate from './pages/admin/product/ProductUpdate';
import ColorCreate from './pages/admin/color/ColorCreate';

//prodcut page
import Product from './pages/Product';


//Components
import Header from './components/nav/Header';


//Category home page
import CategoryHome from './pages/category/CategoryHome'

//subactegory home page
import SubCategoryHome from "./pages/subcategory/SubCategoryHome"


//Shop page
import Shop from './pages/Shop';

//Cart Page
import Cart from './pages/Cart';

//Side Drawer Component
import SideDrawer from './components/drawer/SideDrawer';

//checkout page
import Checkout from './pages/Checkout'


//coupon page
import CreateCouponPage from './pages/admin/coupons/CreateCouponPage'

//payment page
import Payment from './pages/Payment'


import {auth} from './firebase'
import { useDispatch } from 'react-redux';
import {currentUser} from '../src/functions/auth'

const  App = ()  => {

  const dispatch = useDispatch()

  //to check the firebase auth state
  useEffect(()=>{
      const unsubscribe = auth.onAuthStateChanged(async (user) => {
        if(user)
        {
          const idTokenResult = await user.getIdTokenResult()

          console.log("user",user)

          currentUser(idTokenResult.token)
          .then((res) => {
            dispatch({
              type:'LOGGED_IN_USER',
              payload:{
                name: res.data.name,
                email: res.data.email,
                token: idTokenResult.token,
                role: res.data.role,
                _id: res.data._id
              }
            })
          }).catch((err) => {
            console.log(err)
          })
         
        }

      });

      //cleanup
      return () => unsubscribe()

  },[dispatch])

  return(
      <>
        <Header />
        <SideDrawer />
        <ToastContainer />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/register/complete" component={RegisterComplete} />
          <Route exact path="/forgot/password" component={ForgotPassword} />

          <Route exact path="/aboutus" component={AboutUs} />
          <Route exact path="/services" component={Service} />

          <UserRoute exact path="/user/history" component={History} />
          <UserRoute exact path="/user/password" component={Password} />
          <UserRoute exact path="/user/whishlist" component={Whishlist} />
          <UserRoute  exact path="/payment" component={Payment} />

          <AdminRoute exact path="/admin/dashboard" component={AdminDashboard} />
          <AdminRoute exact path="/admin/category" component={CategoryCreate} />
          <AdminRoute exact path="/admin/category/:slug" component={CategoryUpdate} />
          <AdminRoute exact path="/admin/color" component={ColorCreate} />

          <AdminRoute exact path="/admin/subcategory" component={SubCategoryCreate} />
          <AdminRoute exact path="/admin/subcategory/:slug" component={SubCategoryUpdate} />

          <AdminRoute exact path="/admin/product" component={ProductCreate} />
          <AdminRoute exact path="/admin/products" component={AllProducts} />
          <AdminRoute exact path="/admin/products/:slug" component={ProductUpdate} />

          <AdminRoute exact path="/admin/coupon" component={CreateCouponPage} />

          <Route exact path="/product/:slug" component={Product} />
          <Route exact path="/category/:slug" component={CategoryHome} />
          <Route exact path="/subcategory/:slug" component={SubCategoryHome} />
          <Route exact path="/shop" component={Shop} />
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/checkout" component={Checkout} />

          
        </Switch>
      </>
  )
}

export default App;
