import axios from "axios";


export const createColor = async (color,authtoken) =>{
    return await axios.post(`${process.env.REACT_APP_API}/color`,color,{
        headers:{
            authtoken
        }
    })
}

export const updateColor = async (slug,color,authtoken) =>{
    return await axios.put(`${process.env.REACT_APP_API}/color/${slug}`,color,{
        headers:{
            authtoken
        }
    })
}

export const removeColor = async (slug,authtoken) =>{
    return await axios.delete(`${process.env.REACT_APP_API}/color/${slug}`,{
        headers:{
            authtoken
        }
    })
}

export const getColor = async (slug) =>{
    return await axios.get(`${process.env.REACT_APP_API}/color/${slug}`)
}

export const getColors = async () =>{
    return await axios.get(`${process.env.REACT_APP_API}/colors`)
}