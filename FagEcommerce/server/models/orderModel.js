const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema


const OrderSchema = new mongoose.Schema({
    products:[{
        product:{
            type:ObjectId,
            ref:"Product"
        },
        count:{
            type:Number
        },
        color: String,
    },
    ],
    paymentIntent:{},
    orderStatus:{
        type:String,
        default: 'No Procesado',
        enum:[
            "No Procesado",
            "Pago en efectivo",
            "Procesando",
            "Enviado",
            "Cancelado",
            "Completado"
        ]
    },
    orderBy:{
        type:ObjectId,
        ref:"User"
    }
   
},{timestamps: true})

module.exports = mongoose.model("Order",OrderSchema)