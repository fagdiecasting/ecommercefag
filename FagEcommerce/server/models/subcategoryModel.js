const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const subcategorySchema = new mongoose.Schema({
    name:{
        type:String,
        trim:true,
        required:'El nombre es requerido',
        minlength:[3,"Demasiado Corto"],
        maxlength:[40,"Demasiado largo"]
    },slug:{
        type:String,
        unique:true,
        lowercase:true,
        index:true

    },
    parent:{
        type:ObjectId,
        ref: "Category",
        required:true
    }
},{timestamps:true}
)

module.exports = mongoose.model('SubCategory',subcategorySchema);
