const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema


const colorSchema = new mongoose.Schema({
    name:{
        type:String,
        trim:true,
        required: true,
        minlength:[3,"Demasiado Corto"],
        maxlength:[15,"Demasiado Largo"]
    },
    slug:{
        type:String,
        unique:true,
        lowercase:true,
        index:true
    }
},{timestamps: true})

module.exports = mongoose.model('Color',colorSchema)