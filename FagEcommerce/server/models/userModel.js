const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        index: true
    },
    role:{
        type: String,
        default: 'subscriber',

    },
    cart:{
        type:Array,
        default:[],
    },
    address:{
        type:String,
        required: false
    },
    phoneNumber:{
        type:String,
        required:false
    },
    country:{
        type:String,
        required: false
    },
    state:{
        type: String,
        required: false
    },
    //color: String,
    city:{
        type:String,
        required: false
    },
    postalCode:{
        type: Number,
        required: false
    },
    client:{
        type:String,
        required: false
    },
   whishlist:[{type: ObjectId, ref:"Product"}],

},{timestamps: true});

module.exports = mongoose.model('User',userSchema)


