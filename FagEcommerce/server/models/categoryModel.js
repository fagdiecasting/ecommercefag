const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const categorySchema = new mongoose.Schema({
    name:{
        type:String,
        trim:true,
        required:'El nombre es requerido',
        minlength:[3,"Demasiado Corto"],
        maxlength:[40,"Demasiado largo"]
    },slug:{
        type:String,
        unique:true,
        lowercase:true,
        index:true

    }
},{timestamps:true});

module.exports = mongoose.model('Category',categorySchema);

