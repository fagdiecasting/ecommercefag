const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const productSchema = new mongoose.Schema({
    title:{
        type:String,
        required: true,
        trim:true,
        maxlength:50,
       text: true
    },
    slug:{
        type:String,
        unique: true,
        lowercase:true,
        index:true,
       
    },
    description:{
        type:String,
        required:true,
        maxlength:2500,
        text:true
    },
    price:{
        type:Number,
        required:true,
        trim:true,
        maxlength:32,

    },
    category:{
        type:ObjectId,
        ref:"Category"
    },
    subcategories:[
        {
        type:ObjectId,
        ref:"SubCategory"
    },
    ],
    quantity:{
        type:Number
    },
    sold:{
        type:Number,
        default:0
    },
    images:{
        type:Array,
    },
    shipping:{
        type: String,
        enum:["Si","No"]
    },
    color:{
        type:String,
        //enum:["Gris","Plateado","Dorado","Verde","Blanco","Negro","Rojo","Azul","Azul Obscuro"]
        enum:["Color Base"]
    },
    /*colors:[{
        type:ObjectId,
        ref:"Color"
    }],*/
    brand:{
        type:String,
        enum:["Marca 1","Marca 2","Marca 3","Marca 4"]
    },
    ratings:[{
        star:Number,
        postedBy:{type:ObjectId,ref:"User"},
        comment: String
        }
    ],
    reviews:[
        {
            ratings:{
                star:Number,
                postedBy:{type:ObjectId,ref:"User"}
            },
            comment:{
                type:String,
                maxlength: 1000,
                postedBy:{type:ObjectId,ref:"User"}
            }
        }
    ]

},{timestamps:true});

module.exports = mongoose.model("Product",productSchema);