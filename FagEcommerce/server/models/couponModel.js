const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const CouponSchema = new mongoose.Schema({
    
    name:{
        type:String,
        trim:true,
        unique: true,
        uppercase:true,
        required: 'Coupon Name is required',
        minlength:[6,"Demasiado Corto"],
        maxlength: [12,'Demasiado Largo']
    },
    expiry:{
        type: Date,
        required: true
    },
    discount:{
        type: Number,
        required: true
    }


},{timestamps: true})

module.exports = mongoose.model("Coupon",CouponSchema)