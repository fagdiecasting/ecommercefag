const User = require("../models/userModel")
const Cart = require("../models/cartModel")
const Coupon = require("../models/couponModel")
const Product = require("../models/productModel")
const stripe = require("stripe")(process.env.STRIPE_SECRET)

exports.createPaymentIntent = async (req,res) => {

    //console.log(req.body)
    //return;
    const {couponApplied} = req.body

    //later apply coupon
    //later calculate price


    //1 find user
    const user = await User.findOne({email: req.user.email}).exec()


    //2 get user cart total
    const {cartTotal,totalAfterDiscount} = await Cart.findOne({orderBy: user._id }).exec()

    //console.log("CART TOTAL CHARGED ---->>",cartTotal)

    console.log("CART TOTAL CHARGED ---->>",cartTotal, "AFTER DISCCOUNT --> ",totalAfterDiscount)
    //return;

    let finalAmount = 0

    if(couponApplied && totalAfterDiscount)
    {
        //finalAmount = Math.round(totalAfterDiscount * 100)
        finalAmount = totalAfterDiscount * 100

    }else{

        //finalAmount = Math.round(cartTotal * 100)
        finalAmount = cartTotal * 100

    }



    //create payment intent with ordre amount and currency
    const paymentIntent = await stripe.paymentIntents.create({
        /*amount: cartTotal * 100,*/
        amount: finalAmount,
        currency: 'mxn'
    });

    res.send({
        clientSecret: paymentIntent.client_secret,
        cartTotal,
        totalAfterDiscount,
        payable: finalAmount,
    });

}