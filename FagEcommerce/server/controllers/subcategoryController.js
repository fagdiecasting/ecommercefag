const SubCategory = require("../models/subcategoryModel")
const Product = require("../models/productModel")
const slugify = require('slugify')

exports.create = async (req,res,next) => {
    try{

        const {name,parent} = req.body

        const subcategory = await new SubCategory({name,parent,slug:slugify(name)}).save()

        res.json(subcategory)

    }catch(err)
    {   
        console.log('SUB CREATE ERROR',err)
        res.status(400).send('Create SubCategory Failed')
    }
}

exports.read = async (req,res,next) => {

    let subcategory = await SubCategory.findOne({slug: req.params.slug}).exec()

    let products = await Product.find({subcategories: subcategory}).populate("category").exec()

    res.json({subcategory,products})

}

exports.update = async (req,res,next) => {
    const {name,parent} = req.body

    try{

        let updated = await SubCategory.findOneAndUpdate({slug: req.params.slug},{name:name,parent,slug:slugify(name)},{new:true})

        res.json(updated)

    }catch(err)
    {
        res.status(400).send('SubCategory Update Failed')
    }


}

exports.remove = async (req,res) => {
    try{

        const deleted = await SubCategory.findOneAndDelete({slug: req.params.slug});

        res.json(deleted)

    }catch(err)
    {
        res.status(400).send('Delete SubCategory Failed')
    }
}

exports.list = async (req,res,next) => {
    res.json(await SubCategory.find({}).sort({createdAt: -1}).exec());
}