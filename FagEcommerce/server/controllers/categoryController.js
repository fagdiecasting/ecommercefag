const Category = require("../models/categoryModel")
const SubCategory = require("../models/subcategoryModel")
const Product = require("../models/productModel")
const slugify = require('slugify')

exports.create = async (req,res,next) => {
    try{

        const {name} = req.body

        const category = await new Category({name,slug:slugify(name)}).save()

        res.json(category)

    }catch(err)
    {
        res.status(400).send('Create Category Failed')
    }
}

exports.read = async (req,res,next) => {
    let category = await Category.findOne({slug: req.params.slug}).exec()

    //const products = await Product.find({category: category}).populate('category').populate('postedBy','_id name').exec()

    //const products = await Product.find({category: category}).populate('category').populate("subcategories","-_id name").exec()

    const products = await Product.find({category: category}).populate('category').exec()

    res.json({category,products})

}

exports.update = async (req,res,next) => {
    const {name} = req.body

    try{

        let updated = await Category.findOneAndUpdate({slug: req.params.slug},{name:name,slug:slugify(name)},{new:true})

        res.json(updated)

    }catch(err)
    {
        res.status(400).send('Category Update Failed')
    }


}

exports.remove = async (req,res) => {
    try{

        const deleted = await Category.findOneAndDelete({slug: req.params.slug});

        res.json(deleted)

    }catch(err)
    {
        res.status(400).send('Delete Category Failed')
    }
}

exports.list = async (req,res,next) => {
    res.json(await Category.find({}).sort({createdAt: -1}).exec());
}

exports.getSubcategories =  async (req,res) => {
    
    let sub = await SubCategory.find({parent: req.params._id}).exec();

    res.json(sub)
}