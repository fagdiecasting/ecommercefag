const Color = require('../models/colorModel')
const slugify = require('slugify')
const Product = require('../models/productModel')

exports.createColor = async (req,res) => {

    try{

        const {name} = req.body

        const color = await new Color({name,slug:slugify(name)}).save()

        res.json(color)

    }catch(err)
    {
        res.status(400).send('Create Color Failed')
    }
}

exports.updateColor = async (req,res) =>{

    const {name} = req.body

    try{

        const color = await Color.findOneAndUpdate({slug: req.params.slug},{name:name,slug:slugify(name)},{new:true}).exec()

        res.json(color)

    }catch(err)
    {
        res.json(400).send('Update Color Failed')
    }
}

exports.getColor = async (req,res) => {

   

    try{

        const color = await Color.findOne({slug: req.params.slug}).exec()

        const products = await Product.find({colors: color}).populate('color').exec()

        res.json(products)

    }catch(err)
    {
        console.log("Get Color ERROR",err)
    }


}


exports.listColors = async (req,res) =>{

    try{

        const colors = await Color.find({}).sort({createdAt: -1}).exec()

        res.json(colors)


    }catch(err)
    {
        console.log("List Colors Error",err)
    }


}


exports.removeColor = async (req,res) => {

    try{

        const color = await Color.findOneAndDelete({slug: req.params.slug}).exec()

        res.json(color)

    }catch(err)
    {
        console.log("Color Delete Error",err)
    }


}