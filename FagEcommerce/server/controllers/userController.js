const User = require("../models/userModel")
const Product = require("../models/productModel")
const Cart = require("../models/cartModel")
const Coupon = require("../models/couponModel")
const Order = require("../models/orderModel")
const uniqueid = require('uniqueid')
const { v4: uuidv4 } = require('uuid');

exports.userCart = async (req,res) => {

    try{

        console.log(req.body)

        const {cart} = req.body

        let products = []

        const user = await User.findOne({email: req.user.email}).exec();

        //check if cart is logged in user with id already exist
        let cartExistByThisUser = await Cart.findOne({orderBy: user._id}).exec()

        if(cartExistByThisUser)
        {
            cartExistByThisUser.remove()
            console.log("REMOVED OLD CART")
        }


        for(let i =0;i<cart.length;i++)
        {
            let object = {}

            object.product = cart[i]._id
            object.count = cart[i].count
            object.color = cart[i].color

            //get price to creating total
            //let {price} = await Product.findById(cart[i]._id).select('price').exec()
            let productFromDb = await Product.findById(cart[i]._id).select('price').exec()

    
            object.price = productFromDb.price


            products.push(object)

        }


        console.log("PRODUCTS ON CART",products)


        let cartTotal = 0

        for(let i=0;i<products.length;i++)
        {
            cartTotal = cartTotal + products[i].price * products[i].count //get the total of the cart in price
        }

        console.log("CART TOTAL".cartTotal)



        let newCart = await new Cart({
            products:products,
            cartTotal: cartTotal,
            orderBy: user._id

        }).save()

        res.json({ok: true})

        console.log("NEW CART  -----> ",newCart)


    }catch(err)
    {
        console.log("USER CART SAVE ERROR",err)
    }

}


exports.getUserCart = async (req,res) =>{
    try{

        const user = await User.findOne({email: req.user.email}).exec()

        const cart = await Cart.findOne({orderBy: user._id}).populate('products.product','_id title price totalAfterDiscount').exec()

        const {products,cartTotal,totalAfterDiscount} = cart

        res.json({products,cartTotal,totalAfterDiscount}) //req.data.products

    }catch(err)
    {
        console.log("GET USER CART ERROR ---> ",err)
    }
}

exports.emptyCart = async (req,res) =>{
    try{


        const user = await User.findOne({email: req.user.email}).exec()

        const cart = await Cart.findOneAndRemove({orderBy: user._id}).exec()

        res.json(cart)

    }catch(err)
    {
        console.log("EMPRY CART ERROR",err)
    }
}

exports.saveAddress = async(req,res) =>{

    try{

       // const {address,postalCode,country,state,city,phoneNumber,client} = req.bod

       //const {address,data} = req.body

       const updated = await User.findOneAndUpdate({email: req.user.email},req.body,{new: true}).exec();
        

        res.json(updated)

    }catch(err)
    {
        console.log("SAVE ADDRES ERROR ---> ",err)
    }
}

exports.applyCouponToUserCart = async (req,res) =>{

    try{

        const {coupon} = req.body
        console.log("USER COUPON --->",coupon)

        const validCoupon = await Coupon.findOne({name: coupon}).exec()

        if(validCoupon === null)
        {
            return res.json({
                err: 'Cupón Inválido'
            })
        }

        console.log("Cupón Válido",validCoupon)

        const user = await User.findOne({email: req.user.email}).exec()

        let {products,cartTotal} = await Cart.findOne({orderBy: user._id}).populate('products.product',"_id title price").exec()

        console.log("CART TOTAL----> ",cartTotal, "DISCOUNT % TO APLLY ---> ",validCoupon.discount)

        //calculate Total after dsicount

        let totalAfterDiscount = (cartTotal - (cartTotal * validCoupon.discount) / 100).toFixed(2);

        Cart.findOneAndUpdate({orderBy: user._id},{totalAfterDiscount: totalAfterDiscount},{new:true}).exec()

        res.json(totalAfterDiscount);

    }catch(err)
    {
        console.log("COUPON APPLY ERROR ---> ",err)
    }

}

exports.createOrder = async (req,res) => {

    try{

        /*console.log(req.body)
        return;*/

        const {paymentIntent} = req.body.stripeResponse

        const user = await User.findOne({email: req.user.email}).exec()


        let {products} = await Cart.findOne({orderBy: user._id}).exec()

        let newOrder = await new Order({products,paymentIntent,orderBy: user._id}).save()


        //decrement product quentity, increment sold
        let bulkOption = products.map((item) => {
            return{
                updateOne:{
                    filter:{_id: item.product._id}, //IMPORTANT item.product
                    update: {$inc: {quantity: -item.count,sold: +item.count}},
                    
                },

               
            }

            //console.log("PRODICTY SOLD --> ",item.sold)
        })

        let updated  = await Product.bulkWrite(bulkOption,{new: true})
        console.log("PRODUCT QUANTITY-- AND SOLD++ --> ",updated)

        console.log("CREATE ORDER SAVE -->",newOrder)

        res.json({ok: true})

    }catch(err)
    {
        console.log("CREATE ODER FAILED --> ",err)
    }
}

exports.orders = async (req,res) =>{

    try
    {

        let user = await User.findOne({email: req.user.email}).exec()

        let userOrders = await Order.find({orderBy: user._id}).populate('products.product').exec()

        res.json(userOrders);

    }catch(err)
    {
        console.log("GET USER ORDERS ERROR ---> ",err)
    }

}

//whishlist logic

exports.addToWhishlist = async(req,res) =>{

    try{

        const {productId} = req.body


        const user = await User.findOneAndUpdate({email: req.user.email},{$addToSet:{whishlist: productId}}).exec()


        res.json({ok: true})

    }catch(err)
    {
        console.log("ADD TO WHISHLIST ERROR ---> ",err)
    }

}


exports.whishlist = async(req,res) =>{

    try{

        const list = await User.findOne({email: req.user.email}).select("whishlist").populate("whishlist").exec()

        res.json(list)

    }catch(err)
    {
        console.log("GET WHISHLIST ERROR --->",err)
    }

}



exports.removeFromWhishlist = async(req,res) =>{

    try{

        const {productId} = req.params

        const user = await User.findOneAndUpdate({email: req.user.email},{$pull:{whishlist: productId}}).exec()

        res.json({ok:true})

    }catch(err)
    {
        console.log("REMOVE WHISHLIST ERROR --->",err)
    }

}

exports.createCashOrder = async (req,res) => {

    try{

        /*console.log(req.body)
        return;*/

        const {cod,couponApplied} = req.body

        // let paymentIntent = {
        //     id: uniqueid(),
        //     amount: 
        // }

        //if cod is true, create order with status of cash delivery

        if(!cod)
        {
            return res.status(400).send('La opción para pagar en efectivo falló, inténtalo más tarde')
        }

        const user = await User.findOne({email: req.user.email}).exec()


        let userCart = await Cart.findOne({orderBy: user._id}).exec()


        let finalAmount = 0


        //this is to aplied coupon discount
        if(couponApplied && userCart.totalAfterDiscount)
        {
            //finalAmount = Math.round(totalAfterDiscount * 100)
            finalAmount = userCart.totalAfterDiscount * 100

        }else{

            //finalAmount = Math.round(cartTotal * 100)
            finalAmount = userCart.cartTotal * 100

        }


        let newOrder = await new Order(
            {
            products: userCart.products,
            paymentIntent:{
                id: uuidv4(),
                amount: finalAmount,
                currency:'mxn',
                status:"Pago en efectivo",
                created: Date.now() / 1000,
                payment_method_types:['cash']
            },
            orderBy: user._id,
            orderStatus:'Pago en efectivo'
        }).save()


        //decrement product quentity, increment sold
        let bulkOption = userCart.products.map((item) => {
            return{
                updateOne:{
                    filter:{_id: item.product._id}, //IMPORTANT item.product
                    update: {$inc: {quantity: -item.count,sold: +item.count}},
                    
                },

               
            }

            //console.log("PRODICTY SOLD --> ",item.sold)
        })

        let updated  = await Product.bulkWrite(bulkOption,{new: true})
        console.log("PRODUCT QUANTITY-- AND SOLD++ --> ",updated)

        console.log("CREATE ORDER SAVE -->",newOrder)

        res.json({ok: true})


    }catch(err)
    {

    }

}