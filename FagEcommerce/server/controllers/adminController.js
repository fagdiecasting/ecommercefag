const User = require("../models/userModel")
const Order = require("../models/orderModel")


exports.orders = async (req,res) =>{
    
    try{

        let orders = await Order.find({}).sort("-createdAt").populate("products.product").exec()

        res.json(orders)

    }catch(err)
    {
        console.log("GET ORDERS  ERROR --->",err)
    }
}

exports.orderStatus = async (req,res) =>{

    //console.log(req.body)
    //continue;

    try{

        const {orderId,orderStatus} = req.body

        let updated = await Order.findByIdAndUpdate(orderId,{orderStatus},{new: true}).exec()

        res.json(updated)

    }catch(err)
    {
        console.log("ORDER STATUS ERROR ---> ",err)
    }
}