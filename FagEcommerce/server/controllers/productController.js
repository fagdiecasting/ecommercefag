const Product = require("../models/productModel")
const User = require("../models/userModel")
const slugify = require('slugify')

exports.create = async (req,res) => {
    try{

        console.log(req.body)

        req.body.slug = slugify(req.body.title)

        const newProduct = await new Product(req.body).save()

        res.json(newProduct)

    }catch(err)
    {
        //res.status(400).send('Create Product Failed')
        console.log(err)

        res.status(400).json({
            err: err.message
        })
    }
}


exports.read = async (req,res) => {
    const product = await Product.findOne({slug: req.params.slug}).populate("category").populate("subcategories").exec()

    res.json(product)
}

exports.update = async (req,res,next) => {
    

    try{

        if(req.body.title)
        {
            req.body.slug =  slugify(req.body.title);

        }

        const updated = await Product.findOneAndUpdate({slug: req.params.slug},req.body,{new: true}).exec();

        res.json(updated);

    }catch(err)
    {
        console.log("PRODCUT UPDATE ERROR -->",err);
        //return res.status(400).send('Category Update Failed')
        res.status(400).json({
            err: err.message
        })
    }


}

exports.remove = async (req,res) => {
    try{

        const deleted = await Product.findOneAndDelete({slug: req.params.slug});

        res.json(deleted)

    }catch(err)
    {
        console.log(err)
        
        res.status(400).send('Product Delete Failed')
    }
}

exports.listAll = async (req,res) => {
    res.json(await Product.find({}).limit(parseInt(req.params.count)).populate('category')
    .populate('subcategories').sort([['createdAt','desc']]).exec());
}


//WITHOUT PAGINATION

/*exports.list = async (req,res) =>{
    try{

        //createdAt/updatedAt, desc/asc, 3
        const {sort,order,limit} = req.body

        const product = await Product.find({}).populate('category').populate('subcategories').sort([[sort,order]]).limit(limit).exec();

        res.json(product) 

    }catch(err)
    {
        console.log(err)
    }
}*/


//WITH PAGINATION
exports.list = async (req,res) =>{

    console.table(req.body)

    try{

        //createdAt/updatedAt, desc/asc, 3
        const {sort,order,page} = req.body

        let currentPage = page || 1
        const perPage = 3

        const products = await Product.find({}).skip((currentPage-1) *  perPage).populate('category').populate('subcategories').sort([[sort,order]]).limit(perPage).exec();


        res.json(products) 

    }catch(err)
    {
        console.log(err)
    }
}




exports.productsCount = async (req,res) => {
    try{

        let total = await Product.find({}).estimatedDocumentCount().exec();

        res.json(total)

    }catch(err){
        console.log(err)
    }
}


exports.productStar = async (req,res) =>{
    try{

        const product = await Product.findById(req.params.productId).exec()

        const user = await User.findOne({email: req.user.email}).exec()

        const {star} = req.body

        //who is updating?
        //check if currently logged in user have already added rating to this product ?
        let existingRatingObject = product.ratings.find((element) => element.postedBy.toString() === user._id.toString())

        //if user havent left rating yet,push it

        if(existingRatingObject === undefined)
        {
            let ratingAdded = await Product.findByIdAndUpdate(product._id,{
                $push: { ratings: {star: star,postedBy: user._id}}
            },{new: true}).exec();

            console.log("RATING ADDED",ratingAdded)

            res.json(ratingAdded)
        }else{
            //if user already left rating, update it
            const ratingUpdated = await Product.updateOne(
                {ratings: {$elemMatch: existingRatingObject},
            
            }, {$set: {"ratings.$.star": star}},

            {new:true}).exec()

            console.log("RATING UPDATED:", ratingUpdated)
            res.json(ratingUpdated)
        }

    }catch(err)
    {
        console.log("RATING SYSTEM ONLY ERROR",err)
    }
}

exports.productReview = async (req,res) => {
    try{

        const product = await Product.findById(req.params.productId).exec();

        const user = await User.findOne({email: req.user.email}).exec()

        const {star} = req.body

        //who is updating?
        //check if currently logged in user have already added rating to this product  ?
        let existingRatingObject = product.reviews.find((element) => element.ratings.postedBy.toString() === user._id.toString())
        let existingCommentObject = product.reviews.find((element) => element.comment.postedBy.toString() === user._id.toString())

        //if user havent left review yet,push it
        if(existingRatingObject ===  undefined)
        {
            let reviewAdded = await Product.findByIdAndUpdate(product._id,{
                $push: {reviews: {ratings: {star: star,postedBy: user._id},comment: {postedBy:user._id}}},
                //$push: {reviews:{comment: {postedBy:user._id}}}

            },{new:true}).exec()

            console.log("REVIEW ADDED",reviewAdded)
            res.json(rewviewAdded)
        }else{
            //if user already left a review , update it

            const reviewUpdated = await Product.updateOne({
                reviews:{ratings:{$elemMatch: existingRatingObject},comment:{$elemMatch: existingCommentObject}}
            },{new: true}).exec()

            console.log("REVIEW  UPDATED:", reviewUpdated)
            response.json(reviewUpdated)
        }

    }catch(err)
    {
        console.log("REVIEW WITH STAR AND COMMENT ERROR",err)
    }
}

exports.listRelated = async (req,res) =>{
    try{

        const product = await Product.findById(req.params.productId).exec()

        const related = await Product.find({
            _id:{$ne: product._id},
            category: product.category

        }).limit(3)
        .populate('category')
        .populate('subcategories')
        .exec()

        res.json(related)

    }catch(err)
    {
        console.log(err)
    }
}

//search / filter
/*const handleQuery = async (req,res,query) => {
    const products = await Product.find({$text: {$search: query}}).populate("category","_id name")
                    .populate("subcategories","_id name").exec()

                    res.json(products)
}


const handlePrice = async (req,res,price) => {
    try{

        let products = await Product.find({
            price:{
                $gte: price[0],
                $lte: price[1],
            },

        }).populate("category","_id name")
        .populate("subcategories","_id name").exec()

        res.json(products)

    }catch(err)
    {
        console.log("HANDLE PRICE ERROR",err)
    }
}

exports.searchFilters = async (req,res) =>{


        const {query,price} = req.body

        if(query)
        {
            console.log("QUERY: ",query)

            await handleQuery(req,res,query);
        }

        //price will be an array .example [0,10] de 0 a 10 por ejemplo
        if(price !== undefined)
        {
            console.log('PRICE ---> ',price)

            await handlePrice(req,res,price)
        }

    

}*/


// SERACH / FILTER

const handleQuery = async (req, res, query) => {
    const products = await Product.find({ $text: { $search: query } })
      .populate("category", "_id name")
      .populate("subcategories", "_id name")
      .exec();
  
    res.json(products);
  };
  
  const handlePrice = async (req, res, price) => {
    try {
      let products = await Product.find({
        price: {
          $gte: price[0],
          $lte: price[1],
        },
      })
        .populate("category", "_id name")
        .populate("subcategories", "_id name")
        .exec();
  
      res.json(products);
    } catch (err) {
      console.log(err);
    }
  };

  const handleCategory = async (req,res,category) =>{
      try{

        let products = await Product.find({category}).populate("category", "_id name")
        .populate("subcategories", "_id name")
        .exec();


        res.json(products)

      }catch(err)
      {
          console.log(err)
      }
  }

  const handleStars =  (req,res,stars) =>{

    Product.aggregate([
        {
        $project:{
            document: "$$ROOT",
            floorAverage:{
                $floor: {$avg: "$ratings.star"}
            }
            /*title:"$title",
            description:"$description",
            averageRating:""*/
        }
     },
     {
         $match:{floorAverage: stars}
     }
    ])
    .limit(12)
    .exec((err,aggregates) => {
        if(err) console.log("AGGREGATES ERROR",err)
        Product.find({_id:aggregates}).populate("category", "_id name")
        .populate("subcategories", "_id name")
        .exec((err,products) => {
            if(err) console.log("PRODUCT AGGREGATE ERROR",err)
            res.json(products)
        })
    })

  }


  const handleSubcategories = async (req,res,subcategories) =>{
        const products = await Product.find({subcategories: subcategories}).populate("category", "_id name")
        .populate("subcategories", "_id name").exec()

        res.json(products)
  }


  const handleShipping = async (req,res,shipping) =>{

    const products = await Product.find({shipping}).populate("category", "_id name")
    .populate("subcategories", "_id name").exec()

    res.json(products)

  }


  const handleColor = async (req,res,color) =>{
    const products = await Product.find({color}).populate("category", "_id name")
    .populate("subcategories", "_id name").exec()

    res.json(products)
  }


  const handleBrand = async (req,res,brand) =>{

    const products = await Product.find({brand}).populate("category", "_id name")
    .populate("subcategories", "_id name").exec()

    res.json(products)

  }
  
  exports.searchFilters = async (req, res) => {
    const { query, price,category,stars,subcategories,shipping,brand,color } = req.body;
  
    if (query) {
      console.log("query --->", query);
      await handleQuery(req, res, query);
    }
  
    // price [20, 200]
    if (price !== undefined) {
      console.log("price ---> ", price);
      await handlePrice(req, res, price);
    }

    if(category)
    {
        console.log("CATEGORY ---> ",category)
        await handleCategory(req,res,category)
    }

    if(stars)
    {
        console.log("STARS ----->",stars)
        await handleStars(req,res,stars)
    }

    if(subcategories)
    {
        console.log("SUBCATEGORIES ---->",subcategories)

        await handleSubcategories(req,res,subcategories);
    }

    if(shipping)
    {
        console.log("SHIPPING ---->",shipping)

        await handleShipping(req,res,shipping)
    }

    if(color)
    {
        console.log("COLOR ---->",color)

        await handleColor(req,res,color)
    }

    if(brand)
    {
        console.log("BRAND ---->",brand)

        await handleBrand(req,res,brand)
    }
    
    

  };
  