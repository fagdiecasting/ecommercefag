const Coupon = require('../models/couponModel')

exports.create = async (req,res) =>{

    try{

        const {name,expiry,discount} = req.body

        const coupon = await new Coupon({name,expiry,discount}).save()

        res.json(coupon)

    }catch(err)
    {
        console.log("COUPON CREATE ERROR ----> ",err)
    }

}

exports.remove = async (req,res) =>{

    try{


        res.json(await Coupon.findByIdAndRemove(req.params.couponId).exec())

    }catch(err)
    {
        console.log("COUPON REMOVE ERROR ----> ",err)
    }

}



exports.list = async (req,res) =>{

    try{

        res.json(await Coupon.find({}).sort({createdAt: -1}).exec())

    }catch(err)
    {
        console.log("COUPON LIST ERROR ----> ",err)
    }

}