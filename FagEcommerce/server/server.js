const express = require('express')
const mongoose = require('mongoose')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')
const fs = require('fs') //filesystem
const dotenv = require("dotenv")
dotenv.config()

//import routes
const authRoutes = require('./routes/auth')
const router = require('./routes/auth')


//app
const app = express()

//db
mongoose.connect(process.env.DATABASE,{
    useNewUrlParser: true,
    useUnifiedTopology:true
})
.then(() => {
    console.log(`DB Connection succesfully`)
})
.catch(err => {
    console.log(`DB Connection Error ${err}`)
})

//middlewares
app.use(morgan("dev"));
app.use(bodyParser.json({limit:"2mb"}));
app.use(cors());

//route middlewares
//app.use("/api",authRoutes);
fs.readdirSync('./routes').map((route) => 
app.use("/api",require('./routes/' + route))
);


//port
const port = process.env.PORT || 8000;

app.listen(port,() => {
    console.log(`Server runnigin port ${port}`);
})

