const express = require('express')
const router = express.Router()

//middlewares
const {authCheck,adminCheck} = require("../middlewares/auth")

//constroller
const {createColor,updateColor,getColor,listColors,removeColor} = require("../controllers/colorController")

//routes
router.post("/color",authCheck,adminCheck,createColor)
router.get("/color/:slug",getColor)
router.put("/color/:slug",authCheck,adminCheck,updateColor)
router.get("/colors",listColors)
router.post("/color/:slug",authCheck,adminCheck,removeColor)

module.exports = router