const express = require('express')
const router = express.Router()

//import controllers
const {createOrUpdateUser,currentUser} = require('../controllers/authController')


//middlewares
const {authCheck,adminCheck} = require('../middlewares/auth')

//Middleware de prueba
/*const myMiddleware = (req,res,next) => {
    console.log("SOY UN MIDDLEWARE")
    next();
}*/

/*router.get("/testing",myMiddleware,(req,res) => {
    res.json({
        data:'MIDDLEWARE SUCCESFULLY'
    })
})*/


//controller
router.post("/create-or-update-user", authCheck, createOrUpdateUser);
router.post("/current-user",authCheck,currentUser);
router.post("/current-admin",authCheck,adminCheck,currentUser);



module.exports = router;
