const express = require('express')
const router = express.Router()

//middlewares
const {authCheck,adminCheck} = require('../middlewares/auth')

//constroller 
const {create,read,update,remove,list,getSubcategories} = require('../controllers/categoryController')

//routes
router.post('/category',authCheck,adminCheck,create)
router.get('/category/:slug',read)
router.put('/category/:slug',authCheck,adminCheck,update)
router.delete('/category/:slug',authCheck,adminCheck,remove)
router.get('/categories',list)
router.get('/category/subcategories/:_id',getSubcategories)

module.exports = router