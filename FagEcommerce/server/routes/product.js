const express = require('express')
const router = express.Router()

//middlewares
const {authCheck,adminCheck} = require('../middlewares/auth')

//constroller 
const {create,read,update,remove,listAll,list,productsCount,productStar,productReview,listRelated,searchFilters} = require('../controllers/productController')

//routes
router.post('/product',authCheck,adminCheck,create)
router.get('/products/total',productsCount)
/*router.get('/product/:slug',read)
router.put('/product/:slug',authCheck,adminCheck,update)*/
router.delete('/product/:slug',authCheck,adminCheck,remove)
router.get('/products/:count',listAll)
router.get('/product/:slug',read)
router.put("/product/:slug",authCheck,adminCheck,update)

//routes to show in the home page
router.post('/products',list);

//with star rating only
router.put("/product/star/:productId",authCheck,productStar)

//with star and comment rating
router.put("/product/review/:productId",authCheck,productReview)

//related products
router.get("/product/related/:productId",listRelated)

//search
router.post("/search/filters",searchFilters)


module.exports = router