const express = require('express')
const router = express.Router()
const {authCheck} = require('../middlewares/auth')

const {userCart,getUserCart,emptyCart,saveAddress,applyCouponToUserCart,createOrder,orders
        ,addToWhishlist,whishlist,removeFromWhishlist,createCashOrder} = require("../controllers/userController")


router.post("/user/cart",authCheck,userCart) //save cart
router.get("/user/cart",authCheck,getUserCart)
router.delete("/user/cart",authCheck,emptyCart)

//router to delivery addres
router.post("/user/address",authCheck,saveAddress)

/*router.get("/user",(req,res) => {
    res.json({
        data:'USER API endpoint'
    })
});*/

router.get("/user/orders",authCheck,orders)

//user coupon
router.post("/user/cart/coupon",authCheck,applyCouponToUserCart)
router.post("/user/order",authCheck,createOrder)
router.post("/user/order-cash",authCheck,createCashOrder) //cashon delivery


//whislist 
router.post("/user/whishlist",authCheck,addToWhishlist)
router.get("/user/whishlist",authCheck,whishlist)
router.put('/user/whishlist/:productId',authCheck,removeFromWhishlist)


module.exports = router;